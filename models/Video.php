<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property integer $idDom
 * @property string $link
 * @property string $name_video
 * @property integer $status_id
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id'], 'required'],
            [['id', 'idDom', 'status_id'], 'integer'],
            [['link', 'name_video'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idDom' => 'Адрес дома',
            'link' => 'Ссылка Yuotube',
            'name_video' => 'Название Video',
            'status_id' => 'Статус',
        ];
    }
}
