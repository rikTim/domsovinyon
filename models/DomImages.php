<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dom_images".
 *
 * @property integer $id
 * @property integer $idDom
 * @property string $path
 */
class DomImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dom_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDom','number'], 'integer'],
            [['path'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idDom' => 'Дом',
            'path' => 'Картинка',
            'text' => 'Текст',
            'number'=>'Порядок вывода'
        ];
    }

    public function getDom()
    {
        return $this->hasOne(Dom::className(), ['id' => 'idDom']);
    }
}
