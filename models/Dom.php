<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dom".
 *
 * @property integer $id
 * @property string $tech_data
 * @property string $text
 * @property string $avatarId
 */
class Dom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text','active','tech_data', 'avatarId','sales'], 'required'],
            [['text','price','material_house','material_roof','material_windows','material_doors','material_wall','area','eart','address','dop_info','post_adress','readiness','poverh'], 'string'],
            [['tech_data', 'avatarId'], 'string', 'max' => 1000],
            [['geo_lat', 'geo_lng'], 'string', 'max' => 45],
            [['active','sales','promo'],'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tech_data' => 'Подробное описание',
            'text' => 'Текст',
            'avatarId' => 'Фото ID',
            'price' => 'Стоимость',
            'material_house' => 'Материал стен',
            'material_roof' => 'Материал крыши',
            'material_windows' => 'Материал окон',
            'material_doors' => 'Материал дверей',
            'material_wall' => 'Материал внутр стен',
            'area' => 'Площадь дома',
            'eart' => 'Земельный участок',
            'address' => 'Короткий адрес',
            'dop_info' => 'Дополнительная информация(для главной стр)',
            'geo_lat' => 'Координата 1',
            'geo_lng' => 'Координата 2',
            'post_adress' => 'Почтовый адрес',
            'readiness' => 'Готовность',
            'poverh' => 'Этажей',
            'active' => 'Активна',
        ];
    }


    public function getAvatar()
    {
        return $this->hasOne(DomImages::className(), ['id' => 'avatarId']);
    }
    public function getVideo()
    {
        return $this->hasMany(Video::className(), ['idDom' => 'id']);
    }
}
