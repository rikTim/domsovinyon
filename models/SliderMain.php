<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider_main".
 *
 * @property integer $id
 * @property string $images
 * @property string $header
 * @property integer $stutus_id
 */
class SliderMain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_main';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'stutus_id'], 'integer'],
            [['images', 'header'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'images' => 'Изображение',
            'header' => 'Заголовок',
            // 'stutus_id' => 'Stutus ID',
        ];
    }

    public function getAvatar()
    {
        return $this->hasOne(DomImages::className(), ['id' => 'images']);
    }
}
