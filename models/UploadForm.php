<?php
/**
 * Created by PhpStorm.
 * User: kolia
 * Date: 10.08.16
 * Time: 14:32
 */

namespace app\models;

use Imagine\Image\ManipulatorInterface;
use yii\base\Model;
use yii\helpers\BaseFileHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Imagick\Imagine;

class UploadForm extends Model
{


    public $imageFile;
    public $files;
    public $slider;
    public $panorama;

    public function rules()
    {
        return [
            [['imageFile'], 'image', 'minHeight' => 100, 'minWidth' => 100, 'extensions' => 'png, jpg, jpeg'],

            [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 20,'on'=>'files'],
            [['slider'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 5,'on'=>'slider'],
            [['panorama'], 'image', 'minHeight' => 100, 'minWidth' => 100, 'extensions' => 'png, jpg, jpeg','on'=>'panorama'],
        ];
    }


    protected function autoResize($dir, $imgName ,$width , $height,$type='')
    {
        $dirCropped = $dir . '/cropped/';
        switch($type){
            case 1; $name = 'p_'.$imgName;break;
            case 2; $name = 'f_'.$imgName;break;
            case 3; $name = 'sb_'.$imgName;break;
            case 4; $name = 'sm_'.$imgName;break;
            default : $name = $imgName;
        }
        BaseFileHelper::createDirectory($dirCropped);
        Image::thumbnail($dir . '/' . $imgName, $width, $height, ManipulatorInterface::THUMBNAIL_INSET)
            ->save( $dirCropped . $name);

    }



    public function avatar($id)
    {

        if ($this->imageFile && $this->validate()) {


            $dir = 'img/dom/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;
            $name = $imgName;

            /**
             * upload photo
             */
            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            /**
             * main photo
             */
//            $dirCropped = $dir . '/cropped';
            $this->autoResize( $dir, $imgName ,320,200);
            /**
             * more main photo
             */
//            $imgName = 'p_'.$imgName;
            $this->autoResize( $dir, $imgName ,550,350, 1);

            /**
             * footer photo
             */
//            $imgName = 'f_'.$name;
            $this->autoResize( $dir, $imgName ,50,34,2);

            /**
             * slider big photo
             */
//            $imgName = 'sb_'.$name;
//            $this->autoResize( $dir, $imgName ,1200,400,3);

            /**
             * slider small photo
             */
//            $imgName = 'sm_'.$name;
//            $this->autoResize( $dir, $imgName ,240,140,4);

            $photoModel = new DomImages();
            $photoModel->setAttributes([
                'path' => $name,
                'idDom' => $id,
//                'type' => 1,
            ]);
            $photoModel->save();

            $model = new Dom();
            $model->updateAll(['avatarId' => $photoModel->id], 'id  =' . $id);
            return $photoModel->id;
        } else {
            return false;
        }
    }


    public function photos($id)
    {
        $this->scenario = 'files';
        if ($this->validate()) {
            foreach ($this->files as $file) {
                $dir = 'img/dom/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                $this->autoResize($dir, $imgName,120,100);

                $photoModel = new DomImages();
                $photoModel->setAttributes([
                    'path' =>  $imgName,
                    'idDom' => $id,

                ]);
                $photoModel->save();
            }
            return true;
        } else {
            return false;
        }
    }


    public function photo($id)
    {
        if ($this->imageFile && $this->validate()) {
            $dir = 'img/dom/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;
            $name = $imgName;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);


            $this->autoResize($dir, $imgName,120,100);


            $photoModel = new DomImages();
            $photoModel->setAttributes([
                'path' => $name,
                'idDom' => $id,
            ]);
            $photoModel->save();


            return true;
        } else {
            return false;
        }
    }


    public function news($id)
    {

        if ($this->imageFile && $this->validate()) {


            $dir = 'img/news/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;
            /**
             * upload photo
             */
            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            /**
             * main photo
             */
            $this->autoResize( $dir, $imgName ,300,300);

            $this -> resize($dir . '/' . $imgName, $dir . '/cropped/mini_' . $imgName, 80, 80);

            $model = new News();
            $model->updateAll(['avatar' => $imgName], 'id  =' . $id);
            return true;
        } else {
            return false;
        }
    }


    public function slider()
    {
//        $this->scenario = 'slider';
//        $this->validate();
//        print_r($this->getErrors());
//        die();
        if ($this->validate()) {
            SliderMain::deleteAll();
            foreach ($this->slider as $file) {
                $dir = 'img/slider/';
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);
                BaseFileHelper::createDirectory($dir . 'cropped/');
                $this -> resize($dir . '/' . $imgName, $dir . '/cropped/sb_' . $imgName, 1200, 550);

                $this -> resize($dir . '/' . $imgName, $dir . '/cropped/sm_' . $imgName, 240, 140);

                $photoModel = new DomImages();
                $photoModel->setAttributes([
                    'path' =>  $imgName,

                ]);
                $photoModel->save();
//                print_r($photoModel->id);
                $sliderModel = new SliderMain();
                $sliderModel->setAttributes([
                    'images' =>  $photoModel->id,
                    'status_id'=>1,

                ]);
//                print_r($sliderModel);
                $sliderModel->save(false);
//                print_r($sliderModel->getErrors());
            }
            return true;
        } else {
            return false;
        }

    }



    public function panorama($id, $idDom)
    {

        if ($this->panorama && $this->validate()) {


            $dir = 'img/panorama/'.$idDom.'/';
            $imgName = md5(time() . $this->panorama->baseName) . '.' . $this->panorama->extension;


            /**
             * upload photo
             */
            BaseFileHelper::createDirectory($dir);
            $this->panorama->saveAs($dir . '/' . $imgName);

            /**
             * main photo
             */
            BaseFileHelper::createDirectory($dir . 'cropped/');
            $this -> resize($dir . '/' . $imgName, $dir . '/cropped/pb_' . $imgName, 4000, 1140);

            $this -> resize($dir . '/' . $imgName, $dir . '/cropped/pa_' . $imgName, 340, 140);

            $this -> resize($dir . '/' . $imgName, $dir . '/cropped/pm_' . $imgName, 140, 80);


            $photoModel = new DomImages();
            $photoModel->setAttributes([
                'path' =>  $imgName,
                'idDom'=>$idDom,
            ]);
            $photoModel->save();

            $model = new Panorama();
            $model->updateAll(['name_image' => $photoModel->id], 'id  =' . $id);
            return true;
        } else {
            return false;
        }
    }


    public function resize($file_input, $file_output, $w_o, $h_o, $percent = false)
    {
        list($w_i, $h_i, $type) = getimagesize($file_input);
        if (!$w_i || !$h_i) {
            echo 'Невозможно получить длину и ширину изображения';
            return;
        }
        $types = array('', 'gif', 'jpeg', 'png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom' . $ext;
            $img = $func($file_input);
        } else {
            echo 'Некорректный формат файла';
            return;
        }
        if ($percent) {
            $w_o *= $w_i / 100;
            $h_o *= $h_i / 100;
        }
        if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
        if (!$w_o) $w_o = $h_o / ($h_i / $w_i);

        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
        if ($type == 2) {
            return imagejpeg($img_o, $file_output, 100);
        } else {
            $func = 'image' . $ext;
            return $func($img_o, $file_output);
        }
    }

    public function oneSlide($id)
    {

        if ($this->imageFile && $this->validate()) {


            $dir = 'img/slider/';
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;


            /**
             * upload photo
             */
            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            /**
             * main photo
             */
            BaseFileHelper::createDirectory($dir . 'cropped/');
            $this -> resize($dir . '/' . $imgName, $dir . '/cropped/sb_' . $imgName, 1200, 550);

            $this -> resize($dir . '/' . $imgName, $dir . '/cropped/sm_' . $imgName, 240, 140);


            $photoModel = new DomImages();
            $photoModel->setAttributes([
                'path' =>  $imgName,

            ]);
            $photoModel->save();

            $model = new SliderMain();
            $model->updateAll(['images' => $photoModel->id], 'id  =' . $id);
            return true;
        } else {
            return false;
        }
    }

}