<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $text
 * @property string $datetime
 * @property string $status_id
 * @property string $title
 * @property integer $category_id
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'datetime', 'status_id', 'title', 'category_id'], 'required'],
            [['text', 'title','avatar','title_facebook', 'url_facebook', 'summary_facebook', 'image_facebook'], 'string'],
            [['category_id', 'active_facebook'], 'integer'],
            [['datetime', 'status_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст статьи',
            'datetime' => 'Дата публикации',
            'status_id' => 'Статус',
            'title' => 'Заголовок',
            'category_id' => 'Категория',
            'avatar' => 'Картинка',
            'summary_facebook' => 'Текстовое описание (краткое)',
            'active_facebook' => 'Кнопка FB',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
