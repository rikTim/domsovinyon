<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "panorama".
 *
 * @property integer $id
 * @property integer $idDom
 * @property string $name_image
 * @property string $header__image
 * @property string $title_image
 * @property string $alt_image
 */
class Panorama extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'panorama';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDom','status_id','header_image','name_image', 'title_image', 'alt_image'], 'required'],
            [['status_id'], 'integer'],
            [['idDom'], 'integer'],
            [['name_image', 'title_image', 'alt_image'], 'string', 'max' => 45],
            [['header_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idDom' => 'Id Dom',
            'name_image' => 'Панорама',
            'header_image' => 'Загаловок картинки',
            'title_image' => 'title SEO Название',
            'alt_image' => 'Alt SEO Название',
            'status_id' => 'Статус',

        ];
    }
    public function getDom()
    {
        return $this->hasOne(Dom::className(), ['id' => 'idDom']);
    }
    public function getAvatar()
    {
        return $this->hasOne(DomImages::className(), ['id' => 'name_image']);
    }
}
