<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dom;

/**
 * DomAdminSearch represents the model behind the search form about `app\models\Dom`.
 */
class DomAdminSearch extends Dom
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['tech_data', 'text', 'avatarId', 'price', 'material_house', 'material_roof', 'material_windows', 'material_doors', 'material_wall', 'area', 'eart', 'address', 'dop_info', 'geo_lat', 'geo_lng', 'post_adress', 'readiness', 'poverh'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dom::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'tech_data', $this->tech_data])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'avatarId', $this->avatarId])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'material_house', $this->material_house])
            ->andFilterWhere(['like', 'material_roof', $this->material_roof])
            ->andFilterWhere(['like', 'material_windows', $this->material_windows])
            ->andFilterWhere(['like', 'material_doors', $this->material_doors])
            ->andFilterWhere(['like', 'material_wall', $this->material_wall])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'eart', $this->eart])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'dop_info', $this->dop_info])
            ->andFilterWhere(['like', 'geo_lat', $this->geo_lat])
            ->andFilterWhere(['like', 'geo_lng', $this->geo_lng])
            ->andFilterWhere(['like', 'post_adress', $this->post_adress])
            ->andFilterWhere(['like', 'readiness', $this->readiness])
            ->andFilterWhere(['like', 'poverh', $this->poverh])
            ->andFilterWhere(['like', 'active', $this->poverh]);

        return $dataProvider;
    }
}
