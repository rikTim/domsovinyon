<?php
/**
 * Created by PhpStorm.
 * User: Zver
 * Date: 13.10.2016
 * Time: 12:01
 */

namespace app\models;

use Yii;


class NewsLast extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'datetime', 'status_id', 'title', 'category_id'], 'required'],
            [['text', 'title'], 'string'],
            [['category_id'], 'integer'],
            [['datetime', 'status_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст статьи',
            'datetime' => 'Дата публикации',
            'status_id' => 'Статус',
            'title' => 'Заголовок',
            'category_id' => 'Категория',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}