<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_info".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $telefone
 */
class UserInfo extends \yii\db\ActiveRecord
{

//    public $captcha;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'telefone'], 'required'],
            [['text'], 'string'],
            [['name', 'telefone','subject'], 'string', 'max' => 255],
            [['email'],'string', 'max' => 45],
//            ['captcha', 'required'],
//            ['captcha', 'captcha']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'text' => 'Описание',
            'telefone' => 'Телефон',
            'email' => 'Email',
            'subject' => 'Тема'
        ];
    }
}
