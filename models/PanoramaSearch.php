<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Panorama;

/**
 * PanoramaSearch represents the model behind the search form about `app\models\Panorama`.
 */
class PanoramaSearch extends Panorama
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idDom'], 'integer'],
            [['name_image', 'header_image', 'title_image', 'alt_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Panorama::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idDom' => $this->idDom,
        ]);

        $query->andFilterWhere(['like', 'name_image', $this->name_image])
            ->andFilterWhere(['like', 'header_image', $this->header_image])
            ->andFilterWhere(['like', 'title_image', $this->title_image])
            ->andFilterWhere(['like', 'alt_image', $this->alt_image]);

        return $dataProvider;
    }
}
