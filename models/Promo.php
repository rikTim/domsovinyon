<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promo".
 *
 * @property integer $id
 * @property string $promo_text
 * @property integer $id_dom
 * @property string $adress_dom
 * @property integer $status
 * @property string $promo_date
 * @property string $promo_photo
 */
class Promo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dom', 'status','slider_home'], 'integer'],
            [['status','slider_home'], 'required'],
            [['promo_textUP','promo_textDown', 'dop_info', 'promo_date', 'promo_photo','text_card','text_more','card_photo','ugol_photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_textUP' => 'Верхний текст (на главной)',
            'promo_textDown' => 'Нижний текст (на главной)',
            'text_card' => 'Текст (открытка "подробнее")',
            'id_dom' => 'Адрес дома',
            'dop_info' => 'Доп условия("подробнее")',
            'status' => 'Статус акции',
            'slider_home' => 'Слайдер на главной стр',
            'promo_date' => 'Дата окончания',
            'promo_photo' => 'Фото (на главной)',
            'card_photo' => 'Фото (в подробнее)',
            'ugol_photo' => 'Фото (мини уголок)',
            'bground_img' => 'Фото (подложка)',
        ];
    }
public $promo;
    public function getPromo()
    {
        return Promo::find()->where(['status'=>1])->all();
    }

    public function promoInfo()
    {
        $promo = $this->getPromo();

        $info = [];
        foreach ($promo as $infos){
            $info = [
                'promo_textUP'=>$infos['promo_textUP'],
                'promo_textDown'=>$infos['promo_textDown'],
                'id_dom'=>$infos['id_dom'],
                'text_card'=>$infos['text_card'],
                'promo_photo'=>$infos['promo_photo'],
                'ugol_photo'=>$infos['ugol_photo'],
                'promo_date'=>$infos['promo_date'],
                'status'=>$infos['status'],
                'card_photo'=>$infos['card_photo'],
                'slider_home'=>$infos['slider_home'],
                'text_more'=>$infos['text_more'],
            ];
        }
        return $info;
    }

}
