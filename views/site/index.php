<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */


$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы, низкие цены дома, котеджиб дома с участком,готовые дома'], 'description');
$this->title = 'DOM SOVINYON | Главная страница';

?>
<section id="home">

    <div class="presentation-container">
        <?php if (!empty($promo) && $promo[0]['slider_home'] == 1) :?>
            <?= $this->render('/layouts/_slider') ?><br><br>
        <?php endif;?>
        <?php if(!empty($promo)) :?>
            <?php
            $data_starta = $promo[0]['promo_date'];
            ?>
            <?php  $script = <<< JS
$('#countdown').timeTo({
    timeTo: new Date(new Date('Mon May 01 2017 09:00:00 GMT+0200 (Финляндия (зима))')),
    displayDays: 2,
    theme: "black",
    displayCaptions: true,
    fontSize: 48,
    captionSize: 14
}); 
JS;
            $this->registerJs($script, yii\web\View::POS_READY); ?>
            <div class="container">
            <div class="row">
                <?= $this->render('promo-block') ?>
            </div>
        <?php else: ?>
            </div>
            <?= $this->render('/layouts/_slider') ?>
        <?php endif;?>

    </div>
    <div class="presentation-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInLeftBig box2">
                    <h1><span class="violet">DOM SOVINYON </span> - строительство и продажа домов в элитном районе Одессы</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="work-container">
        <div class="container">

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'options' => ['class' => 'col-lg-12 col-md-12 col-sm-12 col-xs-12'],
//                    'layout' => "{items}<div class=\"clear\"></div>{pager}",
                'emptyText' => 'Список пуст',
                'summary' => false,
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => 'col-lg-4 col-md-4 col-sm-6 col-xs-12',
                ],
            ]) ?>

        </div>
    </div>
</section>
<?= $this->render('/layouts/_company') ?>
<div class="container">
    <div class="row">
        <?= $this->render('_callme', ['model' => $model]) ?>
    </div>
</div><br><br>
