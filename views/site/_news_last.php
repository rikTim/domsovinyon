<?php

?>

<div class="col-md-4 col-xs-3">
    <a href="<?=\yii\helpers\Url::toRoute(['site/news-more','id'=> $model->id]) ?>">
        <img src="/img/news/<?=$model->id?>/cropped/mini_<?= $model->avatar?>" class="img-responsive center-block img-padder" alt="<?= $model->title?>" title="<?= $model->title?>">
    </a>
</div>

<div class="col-md-8 col-xs-9">
    <h5 datetime="<?= date('d.m.Y', $model->datetime); ?>">
        <a href="<?=\yii\helpers\Url::toRoute(['site/news-more','id'=> $model->id]) ?>" class="text-uppercase">
            <small><?= $model->title?></small>
        </a>
    </h5>
    <p>
        <time datetime="<?= date('d.m.Y', $model->datetime); ?>"><?= date('d.m.Y', $model->datetime); ?></time>
        <br>
    </p>
</div>
<hr>