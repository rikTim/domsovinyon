<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="clearfix"></div>
<div class="testimonials-container hidden-print">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 call-to-action-text wow fadeInLeftBig boxShadow8 box2">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="text-center consultant violet">Бесплатная консультация</h3>
        </div>
        <div class="clearfix"></div>
        <?php
        $form = ActiveForm::begin([
//                    'layout'=>'horizontal',
            'options' => ['class' => 'call-me-form form-register'],
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
//                            'offset' => 'col-sm-offset-4',
                    'wrapper' => 'col-sm-8',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]) ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10 form-group">
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Ваше имя', 'class'=>'form-control text-center'])->label(false) ; ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10 form-group">
            <?= $form->field($model, 'telefone')->textInput(['class'=>'form-control text-center'])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+8 (999) 999 99 99',
            ])->label(false); ?>
        </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10 call-to-action-button">
                <div class="form-group">
                <?= Html::submitButton('ЗАКАЗАТЬ!', ['class' => 'btn big-link-3']) ?>
            </div>
        </div>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>


