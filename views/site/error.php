<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = '(#404)';
?>


<section id="contacty">
    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 wow fadeIn">
                    <i class="fa fa-warning"></i>
                    <h1>(#404) / </h1>
                    <p>Страница не найдена на сайте www.domsovinyon.com</p>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-us-container">
        <div class="container">
            <div class="row box2">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 contact-form wow fadeInLeft">
                        <h3 class="text-uppercase">Страница по указанному в браузера адресу не существует. </h3>
                        <p>Для дальнейшего просмотра сайта пожалуйста перейдите на страницу указанную в главном меню. </p>

                        <div class="thumbnail not-border">
                            <img class="" src="/img/404.png">
                        </div>
                        <p>В случае возникновения дополнительных вопросов Вы можете обратиться к нам по указанным контактам. </p>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 wow fadeInUp">
                        <div class="thumbnail not-border">
                            <img class="" src="/img/table-send.png">
                        </div>
                        <h2 class="black">НАШИ КОНТАКТЫ</h2>
                        <p class="text-uppercase"><i class="fa fa-phone fa-3x color-price"></i>&nbsp;&nbsp;&nbsp;<b class="contact-size color-price"> +38 (067) 654 25 64</b></p>
                        <p class="text-uppercase"><i class="fa fa-envelope fa-2x color-price"></i><em class="contact-size2 color-price">&nbsp;&nbsp;&nbsp;sales@domsovinyon.com</em></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>