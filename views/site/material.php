<?php

use yii\helpers\Html;

$this->title = 'Лучшие материалы';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы'], 'description');
?>

<section id="material">

    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-home"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете найти более подробную информацию</p>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('/layouts/_company') ?>

    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn">
                <div class="col-sm-12 testimonials-title-more wow fadeIn">
                    <h2 class="text-uppercase">ЭКОЛОГИЧЕСКИ ЧИСТЫЕ МАТЕРИАЛЫ В СТРОИТЕЛЬСТВЕ!</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/materials.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left text-center"><br>
                            <span class="text-uppercase"><strong>Используем исключительно экологически чистые материалы в строительстве.</strong></span></p>
                        <p class="text-left">В процессе  строительства использование строительной техники сведено к минимуму. Проектируя и строя дома мы стараемся не только лучшим образом вписать дом в окружающую
                            среду но и сохранить не тронутыми деревья, кустарники и другие растения произрастающие на Вашем земельном участке.
                        </p>
                        <p><b class="text-center text-uppercase">ВЫБИРАЕМ СТРОИТЕЛЬНЫЙ МАТЕРИАЛ</b><br></p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/derevo.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left">
                            <b>Каменный дом</b>, в первую очередь, практичный выбор. Минимальные эксплуатационные расходы, небольшие теплопотери и долгий срок службы - это те факторы, которые заставляют задуматься о строительстве подобного жилища.
                            <br><b>Деревянный дом</b> обычно выбирают люди, ставящие во главу угла экологичность строения. В таком доме приятнее всего отдохнуть от тяжелой рабочей недели, выспаться, получить психологическую разрядку. Деревянные стены поддерживают весьма комфортную для человека атмосферу — оптимальный уровень влажности и воздухообмена.
                            <br><b>Комбинированный дом</b> - дом, который позволяет объединить практичность каменного дома с легкой атмосферой деревянного. Каменный первый этаж дает пространство для практичных решений и дизайнерских экспериментов, а в спальнях деревянного второго этажа сон будет крепок и приятен.
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/penoplast_block.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left">
                            Возведение тяжелого фундамента и толстых стен из кирпича существенно дороже по сравнению со строительством коттеджа из дерева или строительства монолитного дома.<br>
                            Строительство <b>монолитного дома</b> с применением несъемной опалубки дает творческий простор самым смелым архитектурным идеям.
                            В этом материале можно воплотить в жизнь любые архитектурные формы – от теплого гаража для железного любимца, до плывущих линий небольшой загородной резиденции.
                            Отоплением строения можно будет гордиться перед соседями – настолько оно экономично.<br>
                            В <b>деревянном доме</b> более интенсивно происходит обмен и очистка воздуха. Через бревно или брус в закрытом помещении может меняться до 30% воздуха в сутки, а уникальные свойства этих
                            материалов позволяют в сухую погоду отдавать накопленную влагу, а  в  сырую, наоборот, впитывать в себя ее излишки из жилого помещения.<br>
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/krovlya.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left">
                            <b>Дом из теплой керамики</b> - сочетание долговечности, надежности и экологичности. Стены такого дома простоят не одну сотню лет, окупая достаточно высокую их стоимость. Дом из таких блоков не нуждается в дополнительном утеплении (при толщине стены в 51см). Использование в качестве основы для керамических блоков натуральной глины гарантирует отсутствие каких-либо химических примесей в воздухе.
                            Такой дом - долговечное вложение, в котором будет проживать не одно поколение потомков.<br>
                            Черепица сегодня, несомненно, есть лидером среди кровельных материалов. Она представлена в довольно широком ассортименте: металлочерепица, керамическая черепица, цементная черепица, композитная черепица, битумная черепица.
                            Идеальных кровельных материалов нет – все они имеют и свои недостатки. Поэтому изучите все характеристики различных видов черепицы и определитесь, какие из них для вас наиболее важны.
                            <br><b>Итак, определяйтесь!!!</b><br>
                            Если Ваш выбор склоняется в пользу уютного деревянного дома или Вас привлекает надежность и долговечность каменного - обращайтесь в нашу строительную компанию.<br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('_callme', ['model' => $model]) ?>
    </div>
</section>
