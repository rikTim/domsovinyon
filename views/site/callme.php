
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\gbUser $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div id="success"> </div>

<div class="gb-user-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'call-me-big']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 55], ['class' => 'input-modal']) ?>

    <?= $form->field($model, 'telefone')->textInput(['maxlength' => 25], ['class' => 'input-modal']) ?>


    <div class="form-group text-right">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success btn-update-password']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>