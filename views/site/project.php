<?php

use yii\helpers\Html;

$this->title = 'ЭКСКЛЮЗИВНЫЕ ПРОЕКТЫ';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы'], 'description');
?>

<section id="project">

    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-lightbulb-o"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете найти более подробную информацию</p>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('/layouts/_company') ?>

    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn">
                <div class="col-sm-12 testimonials-title-more wow fadeIn">
                    <h2>СТРОИТЕЛЬСТВО ДОМОВ ПО ИНДИВИДУАЛЬНЫМ ПРОЕКТАМ!</h2>
                </div>
                <div class="col-sm-4 wow fadeIn size-text">
                    <img class="thumbnail not-hover" alt="foto" src="/img/blog/project.jpg">
                </div>
                <div class="col-sm-8 wow fadeIn size-text">
                    <p class="text-left text-center">
                        <span class="text-uppercase"><strong>Строительство домов по индивидуальным проектам - основное направление деятельности нашей компании.</strong></span></p>
                    <p class="text-left"><b>Частый наш клиент</b> - человек с листком  клетчатой бумаги, на котором нарисована примерная схема его пожеланий к будущему дому.<br>
                        Бывает, что это просто неоформленные представления о необходимых функциях, которым должен удовлетворять будущий дом.<br>
                        Всегда обсуждаем с заказчиком его видения планировки, предлагаем наиболее интересные и подходящие решения по внешнему виду дома.<br></p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/project-interyer.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <ul class="list text-left">
                            <li>В процессе проектирования наши специалисты стараются учесть все возможные нюансы:</li>
                            <li><i class="fa fa-check"></i> расположение дома на участке с учетом ландшафта,</li>
                            <li><i class="fa fa-check"></i> подъездов,</li>
                            <li><i class="fa fa-check"></i> соседей,</li>
                            <li><i class="fa fa-check"></i> наиболее благоприятную ориентацию комнат по сторонам света,</li>
                            <li><i class="fa fa-check"></i> оптимальное соотношение площади помещений к их высоте, и другие особенности.</li>
                        </ul>
                        <p class="text-left">
                            Идеи заказчика постараемся оформить в оптимальном виде: разработать удобную, практичную планировку и гармоничный экстерьер  и интерьер дома.<br>
                            Индивидуальное строительство отличается от типового строительства малоэтажного строительства тем, что Заказчик на выходе получает дом, отвечающий любым его пожеланиям. И в таком доме можно воплотить свои самые смелые и неожиданные пожелания.
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/project-image.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left">
                            При индивидуальном строительстве дома в нашей компании разрабокта архитектурных решений осуществляются бесплатно. По окончании строительства заказчику выдается паспорт проекта - документ, необходимый для регистрации дома в БТИ.
                        </p>
                        <ul class="list text-left">
                            <li>Состав паспорта проекта:</li>
                            <li><i class="fa fa-check"></i> пояснительная записка с техническими условиями строительства и эксплуатации дома;</li>
                            <li><i class="fa fa-check"></i> планы фундамента, этажей и кровли дома;</li>
                            <li><i class="fa fa-check"></i> фасадные виды и разрезы дома;</li>
                            <li><i class="fa fa-check"></i> цветные перспективные виды дома;</li>
                            <li><i class="fa fa-check"></i> ситуационный план (привязка дома к участку).</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('_callme', ['model' => $model]) ?>
    </div>
</section>
