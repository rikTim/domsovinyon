<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон,контакты']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы'], 'description');
$this->title = 'Контакты';

$this->params['breadcrumbs'][] = $this->title;
?>
<section id="contacty">
    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 wow fadeIn">
                    <i class="fa fa-envelope"></i>
                    <h1>Свяжитесь с нами / </h1>
                    <p>Здесь вы можете связаться с нами</p>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-us-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box2">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 contact-form wow fadeInLeft">
                        <p>
                            Если у Вас возникли вопросы и предложения, можете связаться с нами, заполнив простую форму связи или любым удобным способом указанным в контактах.
                        </p>
                            <?php if (Yii::$app->session->hasFlash('success')): ?>
                            <div class="alert">
                                <h2 class="text-center"><span class="violet">Спасибо за обращение к нам.<br> Мы постараемся ответить вам как можно скорее.</span></h2>
                            </div>
                        </div>
                        <?php else: ?>
                            <?php $form = ActiveForm::begin(['id' => 'contact-form']);?>
                            <div class="form-group">
                                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Как можно к Вам обращаться?'])?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'email')?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'subject')?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'telefone')->widget(\yii\widgets\MaskedInput::className(), [
                                    'mask' => '+8 (999) 999 99 99',
                                ]);?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'text')->textArea(['rows' => 6])?>
                            </div>
                            <?= Html::submitButton('Отправить', ['class' => 'btn', 'name' => 'contact-button'])?>
                            <?php ActiveForm::end();?>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 wow fadeInUp">
                        <div class="thumbnail not-border">
                            <img class="img-responsive" src="/img/table-send.png">
                        </div>
                        <h2 class="black">НАШИ КОНТАКТЫ</h2>
                        <p class="text-uppercase"><i class="fa fa-phone fa-3x color-price"></i>&nbsp;&nbsp;&nbsp;<b class="contact-size color-price"> +38 (067) 654 25 64</b></p>
                        <p class="text-uppercase"><i class="fa fa-envelope fa-2x color-price"></i><em class="contact-size2 color-price">&nbsp;&nbsp;&nbsp;sales@domsovinyon.com</em></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>