<?php

use yii\helpers\Html;

$this->title = 'Наличие документов';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы'], 'description');
?>

<section id="documenty">

    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-leanpub"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете найти более подробную информацию</p>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('/layouts/_company') ?>

    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn">
                <div class="col-sm-12 testimonials-title-more wow fadeIn">
                    <h2>СТРОИТЕЛЬСТВО ДОМОВ ПО ИНДИВИДУАЛЬНЫМ ПРОЕКТАМ!</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/documenty_proto.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left text-center">
                        <span class="text-uppercase"><strong>Прежде чем построить дом на своем участке, необходимо получить ряд разрешительных документов.
                                Мы готовы оказать содействие в этом не легком деле.</strong></span>
                        </p>
                        <p class="text-left">
                            Когда речь идет о строительстве на участке в коттеджном поселке, придется получить разрешение еще и у соседей. А скважина или колодец, на которые не будут получены разрешения, могут создать дополнительные проблемы в процессе оформления частной собственности на дом после окончания строительных работ.
                            Разрешение на строительство невозможно получить, пока не будет проведено топографическое исследование участка и определены возможности строительства на данной территории. Соответственно, необходимо получить акт исследования, на основании которого, в том числе, и будет выдано разрешение на проведение земляных и монтажных работ. Обязательно и наличие акта исследования участка и разбивки границ, исходя из которых, вычисляются возможности возведения здания и других строений на участке.
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/documenty_pro.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left"><br><br>
                            Количество и перечень необходимых бумаг зависит от проекта будущего дома.<br>
                            Если площадь будущего дома будет превышать 400 кв. м, дополнительно потребуется архитектурно-планировочное задание, которое может выдать местный Комитет по градостроению и архитектуре на основании разрешений от санитарной, экологической и пожарной служб.
                        </p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/documenty_profre.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left"><br><br>
                            Срок действия окончательного <b>разрешения на строительство – до 10 лет</b>.<br>
                            Но большинство "промежуточной" документации действительно не более двух.<br> Если у владельца в течение этого времени меняются планы или оказывается недостаточно финансов, придется со временем готовить пакет документов заново.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('_callme', ['model' => $model]) ?>
    </div>
</section>

