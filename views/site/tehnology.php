<?php
use yii\helpers\Html;

$this->title = 'ПЕРЕДОВЫЕ ТЕХНОЛОГИИ';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы'], 'description');
?>

<section id="tehnology">

    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-home"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете найти более подробную информацию</p>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('/layouts/_company') ?>

    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn">
                <div class="col-sm-12 testimonials-title-more wow fadeIn">
                    <h2 class="text-uppercase">Используем передовые европейские строительные технологии!</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-4 wow fadeIn size-text">
                        <img class="thumbnail not-hover" alt="foto" src="/img/blog/tehnology.jpg">
                    </div>
                    <div class="col-sm-8 wow fadeIn size-text">
                        <p class="text-left text-center">
                            <span class="text-uppercase"><strong>Новые технологии в строительстве коттеджей.</strong></span></p>
                        <p class="text-left">В процессе  строительства использование строительной техники сведено к минимуму. Проектируя и строя дома мы стараемся не только лучшим образом вписать дом в окружающую
                            среду но и сохранить не тронутыми деревья, кустарники и другие растения произрастающие на Вашем земельном участке.
                            Технический прогресс благоприятно затронул сферу строительства. Еще полвека назад дома возводились годами с большими трудовыми затратами.
                            Сейчас новые технологии в строительстве коттеджей дают возможность сократить этот процесс минимум в 10 раз.<br>
                            Ниже приведены наиболее популярные новые технологии, применяемые для быстрого строительства.
                        </p>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="keramobloki" src="/img/blog/keramobloki.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p><b class="text-center text-uppercase">Дома из керамоблоков</b><br></p>
                            <p class="text-left">
                                <b>Керамоблок</b> стал прекрасным конкурентом кирпичу. Он имеет микропористую структуру с ребристой боковой поверхностью. Керамические блоки имеют разные размеры,
                                но по высоте они кратны кирпичной кладке, поэтому их удобно использовать. Для его изготовления используется только природный материал: глина, вода, огонь и мелкая древесная стружка.
                                Компоненты, входящие в состав керамоблоков аналогичны кирпичу, но благодаря добавлению древесной стружки появилась пористость, которая сделала его «теплым».
                            </p>
                            <ul class="list text-left">
                                <li>Преимущества керамических блоков следующие:</li>
                                <li><i class="fa fa-check"></i> долговечность;</li>
                                <li><i class="fa fa-check"></i> экологичность;</li>
                                <li><i class="fa fa-check"></i> энергоэффективность;</li>
                                <li><i class="fa fa-check"></i> огнеупорность;</li>
                                <li><i class="fa fa-check"></i> прочность;</li>
                                <li><i class="fa fa-check"></i> высокие звукоизоляционные свойства;</li>
                                <li><i class="fa fa-check"></i> небольшой вес и удобные размеры.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="penobeton" src="/img/blog/penobeton.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p><b class="text-center text-uppercase">Дома из пенобетона</b><br></p>
                            <p class="text-left">
                                Хотя <b>строительство из пеноблоков</b> относится к новейшим технологиям, но этот стройматериал уже давно применяется в строительстве. Пенобетон получают путем смешивания песка, цемента, воды с добавлением пенообразователя, благодаря которому он получает пористую структуру. Как и все стройматериалы, пенобетон имеет свои достоинства и недостатки.    </p>
                            <ul class="list text-left">
                                <li>Преимущества пеноблоков:</li>
                                <li><i class="fa fa-check"></i> высокие теплоизоляционные качества;</li>
                                <li><i class="fa fa-check"></i> невысокая стоимость;</li>
                                <li><i class="fa fa-check"></i> простота укладки и быстрота строительства;</li>
                                <li><i class="fa fa-check"></i> небольшой вес;</li>
                                <li><i class="fa fa-check"></i> экологичность;</li>
                                <li><i class="fa fa-check"></i> высокая пожаробезопасность;</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="monolit" src="/img/blog/monolit.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p><b class="text-center text-uppercase">Монолитное строительство</b><br></p>
                            <p class="text-left">
                                Построить дом по новой технологии можно с использованием <b>съемной и несъемной опалубки</b>, которая применяется при монолитном строительстве.
                                Дома, построенные по этой технологии обладают особой прочностью и могут <em>выдерживать колебания до 8 баллов</em>, что дает возможность строить дома в сейсмически опасных районах.
                            </p>
                            <ul class="list text-left">
                                <li>Преимущество монолитных домов следующие:</li>
                                <li><i class="fa fa-check"></i> прочность;</li>
                                <li><i class="fa fa-check"></i> долговечность;</li>
                                <li><i class="fa fa-check"></i> быстрота возведения;</li>
                                <li><i class="fa fa-check"></i> возможность любой конфигурации и планировки помещений;</li>
                                <li><i class="fa fa-check"></i> тонкие стены позволяют сэкономить на площади.</li>
                            </ul>
                            <p class="text-left">
                                Недостатком является высокая теплопроводность, что требует дополнительной теплоизоляции, низкая звуко- и пароизоляция, высокая стоимость работ.
                                Наша фирма строит монолитные <b>дома под ключ</b>. Качественное утепление домов делает их уютными.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="gazobeton" src="/img/blog/gazobeton.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p><b class="text-center text-uppercase">Коттедж из газобетона</b><br></p>
                            <p class="text-left">
                                <b>Газобетонные блоки</b> изготавливаются из цемента, извести и песка, относятся к ячеистым бетонам, имеющим пористую структуру.
                                Они легко поддаются обработке, поэтому все более завоевывают популярность как строительный материал. Дома из газобетона быстро возводятся,
                                легко перестраиваются,  отделываются и ремонтируются.
                            </p>
                            <ul class="list text-left">
                                <li> Преимущества газобетона следующие:</li>
                                <li><i class="fa fa-check"></i> небольшой вес;</li>
                                <li><i class="fa fa-check"></i> быстрота и удобство укладки;</li>
                                <li><i class="fa fa-check"></i> удобные размеры;</li>
                                <li><i class="fa fa-check"></i> высокая пожаробезопасность;</li>
                                <li><i class="fa fa-check"></i> паропроницаемость;</li>
                                <li><i class="fa fa-check"></i> низкая теплопроводность;</li>
                                <li><i class="fa fa-check"></i> экологическая чистота.</li>
                            </ul>
                            <p class="text-left">
                                Из недостатков этого строительного материала можно называть хрупкость, высокую гигроскопичность и повышенные требования к соблюдению
                                технологии строительства. Pешая вопрос строить самому или с помощью фирмы, лучше отдать предпочтение надежной строительной организации,
                                имеющий опыт такого строительства. Наша фирма имеет достаточно высококвалифицированных специалистов, которые качественно выполняют все
                                работы. Кроме того, <b>на выполненные работы предоставляется гарантия</b>.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="sendvich" src="/img/blog/sendvich.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p><b class="text-center text-uppercase">Строительство из сэндвич-панелей</b><br></p>
                            <p class="text-left">
                                Данная технология широко применяется в <em>коммерческом и жилищном строительстве</em>. Сэндвич-панель представляет собой два металлических листа, внутри
                                которого находится утеплитель. Благодаря пазам они легко монтируются. Для жилых домов в панелях вместо металла применяют спрессованные деревянные опилки
                                со смолой и пенополистиролом в качестве теплоизолятора. Такие дома имеют высокие теплоизоляционные качества и хорошую экологичность.
                                Возведение дома занимает от двух недель до нескольких месяцев.
                            </p>
                        </div>
                    </div>
<div class="hr"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="karkas" src="/img/blog/karkas.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p><b class="text-center text-uppercase">Каркасное строительство</b><br></p>
                            <p class="text-left">
                                Благодаря дешевизне и быстроте возведения все большую популярность приобретает такие технологии строительства частных домов, как каркасное строительство.
                                Технология состоит в том, что деревянный каркас снаружи обшивается OSB плитами, а снаружи любым материалом для внутренней отделки.
                                Если раньше деревянные дома считались недолговечными, то современные каркасные наоборот. Дерево обработано специальными средствами, что продлевает
                                <em>срок эксплуатации строения до 70 лет</em>. У домов с металлическим каркасом <i>срок службы – 100 лет</i>. Если выполнено правильно утепление, то каркасные дома очень теплые и отвечают режиму влажности.
                            </p>
                            <p class="text-left">
                                Недостатком является высокая пожароопасность, но при соблюдении технологии строительства она снижается.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <?= $this->render('_callme', ['model' => $model]) ?>
        </div>
</section>