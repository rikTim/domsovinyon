<?php


//use yii\helpers\Html;

$this->title = 'О нас';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов в элитном районе Одессы'], 'description');
?>

<section id="about">

    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-user"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете найти более подробную информацию о нашей компании</p>
                </div>
            </div>
        </div>
    </div>

    <div class="presentation-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeInLeftBig box2">
                    <h1><span class="violet">DOM SOVINYON </span>, предлагаем красивые дома в лушем районе любимой
                        Одессы.</h1>
                    <p>СОВИНЬОН - лучший район для добрых людей и красивых домов.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn box2">
                    <div class="col-sm-12 testimonials-title-more wow fadeIn">
                        <h2>DOM SOVINYON - ДЕВЕЛОПЕР ЕВРОПЕЙСКОГО УРОВНЯ!</h2>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="foto" src="/img/blog/idea.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p class="text-left text-center"><br>
                                <span class="text-uppercase"><strong>ИСТОРИЯ компании</strong></span></p>
                            <p class="text-left">  Девелоперская компания  была основана в 1995 году.<br>За прошедшее время компания существенно расширила спектр предлагаемых услуг, наработала существенный строительный опыт.
                                <br>Мы постоянно находимся в поиске передовых технологий строительства.<br> Это стремление позволяет нам выполнить практически любые пожелания клиента.</p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="foto" src="/img/blog/mysion.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p class="text-left text-center"><br><br>
                                <span class="text-uppercase"><strong>Миссия компании</strong></span></p>
                            <p class="text-left"> Предоставление девелоперских услуг в лучших Европейских традициях с целью удовлетворения самого взыскательного Заказчика.</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-sm-4 wow fadeIn size-text">
                            <img class="thumbnail not-hover" alt="foto" src="/img/blog/partners.jpg">
                        </div>
                        <div class="col-sm-8 wow fadeIn size-text">
                            <p class="text-left text-center"><br>
                                <span class="text-uppercase"><strong>ПОЧЕМУ ВЫБРАЛИ НАС</strong></span></p>
                            <p class="text-left">Передовые строительные технологии и эффективная дружная команда залог нашего успеха.</p>
                            <ul class="list text-left">
                                <li> Основная сфера ее деятельности - организация и управление строительными проектами:</li>
                                <li><i class="fa fa-check"></i> оформление права землепользования;</li>
                                <li><i class="fa fa-check"></i> определение наиболее эффективного варианта целевого использования земельных участков;</li>
                                <li><i class="fa fa-check"></i> консультации по проектированию;</li>
                                <li><i class="fa fa-check"></i> строительство;</li>
                                <li><i class="fa fa-check"></i> продажа и аренда недвижимости;</li>
                                <li><i class="fa fa-check"></i> управление недвижимостью.</li>
                            </ul>
                            <p class="text-left"> Во все кризисные годы наша компания остается верна своему принципу - строить быстрее, чтобы сократить сроки ответственности перед инвесторами.
                                Качество строительства при этом всегда остается неизменно высоким.
                            </p>
                        </div>
                    </div>
                </div>
            </div
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?= $this->render('_callme', ['model' => $model]) ?>
        </div>
    </div>
</section>