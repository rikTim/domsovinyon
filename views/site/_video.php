<?php

?>


<?php foreach($dom->video as $video):  ?>
    <?php if($video->status_id==2){ ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 video">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?=$video->link?>"></iframe>
            </div>
            <div class = "caption"> <h3><?=$video->name_video?></h3></div>
        </div>
    <?php } ?>
<?php endforeach; ?>