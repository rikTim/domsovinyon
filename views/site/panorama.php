<?php


//use yii\helpers\Html;
//$this->registerJsFile('panorama/js/jquery.cyclotron.js');

use app\models\Panorama;

$this->title = 'Виртуальный тур';

?>

<section id="panorama">
    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-camera"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете найти более подробную информацию о нашей компании</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="presentation-container">

                <div class="col-sm-12 wow fadeInLeftBig box2">
                    <h1><span class="violet">DOM SOVINYON </span>, предлагаем красивые дома в лушем районе любимой
                        Одессы.</h1>
                    <p>СОВИНЬОН - лучший район для добрых людей и красивых домов.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="about-us-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn box2">
                    <ul class="nav nav-tabs">
                        <?php $i=1;
                        foreach ($images as $image):
                            ?>
                            <?php if($i==1){
                            $active = 'active';
                        }else $active= '';
                            ?>
                            <li class="<?=$active?>"><a data-toggle="tab" href="#<?=$i?>">
                                    <p><img src="<?= Yii::getAlias('@web') . '/img/panorama/' . $image->idDom . '/cropped/pm_' . $image->avatar->path ?>"  alt=""  title=""></p>
                                    <h5><?=$image->header_image?></h5>
                                </a></li>
                            <?php $i++; endforeach; ?>
                    </ul>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tab-content boxShadow9">
                            <?php $j = 1;
                            foreach ($images as $image):
                                ?>
                                <?php if($j==1){
                                $active_in = 'in active';
                            }else $active_in= '';

                                ?>
                                <div id="<?=$j?>" class="tab-pane fade <?=$active_in?>">
                                    <div class="panorama"> <img src="<?= Yii::getAlias('@web') . '/img/panorama/' . $image->idDom . '/cropped/pb_' . $image->avatar->path ?>"  alt=""  title=""></div>
                                    <h3 class="text-center"><?=$image->header_image?></h3>
                                </div>
                                <?php $j++; endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





</section>
<div class="container">
    <div class="row">
        <?= $this->render('_callme', ['model' => $model]) ?>
    </div>
</div>

