<?php
use yii\helpers\Html;
use app\models\News;
$this->title = 'БЛОГ';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON, новостной блог']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - новости,статьи,строительный портал,продажа домов'], 'description');
?>

<div id="news">

    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn">
                    <i class="fa fa-book"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?> /</h1>
                    <p>Ниже вы можете прочитать интересную информацию</p>
                </div>
            </div>
        </div>
    </div>
    <section class="blog-title text-center">
        <div class="blog-wrapper section-wrapper">
            <div class="container">
                <div class= "row">
                    <h2 class="headline"><span class="text-uppercase">Наш Блог</span></h2>
                </div>
            </div>
        </div>
    </section>

    <section class= "blog-content">
        <div class= "container">
            <div class= "row box2">

                <main class="blog1col col-lg-9 col-md-9 col-sm-12 col-xs-12">

                    <?= \yii\widgets\ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_news_list',
                        'options' => ['class' => 'archive'],
//                    'layout' => "{items}<div class=\"clear\"></div>{pager}",
                        'emptyText' => 'Список пуст',
                        'itemOptions' => [
                            'tag' => 'article',
                            'class' => 'row',
                        ],
                    ]) ?>

                </main>

                <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="widget widget_categories" id="categories-3">
                        <h4><span>Категории</span></h4>
                        <ul>
                            <?php foreach($category as $cat):  ?>

                                <li class="cat-item cat-item-4">
                                    <?= Html::a($cat->title , 'news?NewsSearch%5Bcategory_id%5D='.$cat->id.'&r=site%2Fnews') ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="widget widget_iw_last_posts" id="iw_last_posts-3">
                        <h4><span>Интересные <br>статьи и изображения:</span></h4>
                        <article>

                            <?= \yii\widgets\ListView::widget([
                                'dataProvider' => $dataProviderLast,
                                'itemView' => '_news_last',
                                'options' => ['class' => 'archive'],
                                'emptyText' => 'Список пуст',
                                'layout' => "{items}<div class=\"clear\"></div>",
                                'itemOptions' => [
                                    'tag' => 'div',
                                    'class' => 'row',
                                ],
                            ]) ?>

                        </article>
                    </div>
                    <div class="widget widget_tag_cloud" id="tag_cloud-3">
                        <h4><span>Тэги</span></h4>
                        <div class="tagcloud">

                            <?php foreach($dataProviderTag->models as $tag):  ?>

                                <?= Html::a($tag->tag_news , 'news?NewsSearch%5Bcategory_id%5D='.$tag->category_id.'&r=site%2Fnews') ?>

                            <?php endforeach; ?>

                        </div>
                    </div>
                </aside>
            </div>
            <?= $this->render('_callme', ['model' => $model]) ?>
        </div>
        <br>
        <br>
    </section>
</div>
