<?php

use yii\helpers\Url;
use app\models\DomImages;
use app\models\Panorama;
use app\models\Promo;
?>

<?php $avatar = DomImages::findOne($model->avatarId);

?>
<div class="work wow fadeInDown">
    <div class="box1">

        <a class="view-work" href="<?= Yii::getAlias('@web') .'/img/dom/' .$model->id .'/cropped/p_'.$avatar->path?>">
            <?php if($model->sales ==2){
                $sales= '';
            }else  $sales= 'sales';
            ?>
            <div class="lenta-wrapper-yellow <?=$sales?>"><div class="lenta-yellow">ПРОДАНО</div></div>
            <?php $infos=Promo::find()->where(['status'=>1,'id_dom'=>$model->id])->one();
            if($infos['status']==1) :?>
<!--                <div class="lenta-action"><img class="image-2" alt="Изображение пометки акция" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$infos['ugol_photo']?><!--"></div>-->
            <?php endif;?>
            <img class="image-2" alt="Изображение дома по адресу <?= $model->address?>" src="<?= Yii::getAlias('@web') .'/img/dom/' .$model->id .'/cropped/'.$avatar->path?>"></a>
        <?php if(!empty($model->price)) :?>
            <h3><?= $model->price?></h3>
        <?php else:?>
            <p class="balkon"></p>
        <?php endif ?>
        <h4 class="view-info-adress"><?= $model->address?></h4>
    </div>
    <div class="pricing-1-box-features">
        <ul>
            <li><span class = "pull-left">&nbsp;&nbsp;<i class = "glyphicon glyphicon-home color-price"></i></span><b><?= $model->area?> м<sup>2</sup></b></li>
            <li><span class = "pull-left">&nbsp;&nbsp;<i class = "glyphicon glyphicon-picture color-price"></i></span><b><?= $model->eart?></b></li>
            <li><span class = "pull-left">&nbsp;&nbsp;<i class = "glyphicon glyphicon-info-sign color-price"></i></span><b>&nbsp;&nbsp;<?= $model->dop_info?></b></li>
        </ul>
    </div>
    <div class="work-bottom">
        <a class="btn-sm big-link-1" href="<?= Url::toRoute(['site/more','id'=>$model->id])?>">ПОДРОБНЕЕ</a>
        <?php $panorama = Panorama::find()->where(['idDom'=>$model->id,'status_id'=>2])->one(); ?>

        <?php if (!empty($panorama)) {?>
            <a class="btn-sm big-link-1" href="<?= Url::toRoute(['site/panorama','id'=>$model->id])?>">3D ТУР &nbsp;<i class="fa fa-eye"></i></a>
        <?php }?>
    </div>
</div>