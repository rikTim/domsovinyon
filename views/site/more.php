<?php

use app\models\DomImages;

$this->title = 'DOM SOVINYON';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'DOM SOVINYON,подробно про дом,лучшие дома в районе Совиньон']);
$this->registerMetaTag(['name' => 'description', 'content' => 'DOM SOVINYON - продажа домов, подробно,описание дома,купить, в элитном районе Одессы'], 'description');

?>

<?php if(!empty($promo[0]['bground_img'])) :?>
    <!--    --><?php //echo <<<END
//
//<style>
//    body{
//        background: url(/img/promo/{$promo[0]['bground_img']}) repeat;
//    }
//</style>
//END;
//    ?>
    <section id="more">

        <div class="page-title-container hidden-print">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn">
                        <i class="fa fa-camera"></i>
                        <h1><?= $this->params['breadcrumbs'][] = $this->title; ?> /</h1>
                        <p>Ниже вы можете ознакомиться с подробной информацией про акцию</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container hidden-print">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn">
                    <div class="col-sm-12 testimonials-title-more wow fadeIn hidden-print">
                        <h2 class="violet"><?=$promo[0]['text_more']?><br>Звоните прямо сейчас:&nbsp; +38 (067) 654 25 64</h2>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="box1">
                                <?php $avatar = DomImages::findOne($dom->avatarId);
                                $files = DomImages::find()->where(['idDom' => $dom->id])->all(); ?>
                                <!--                                <div class="lenta-action2"><img class="" alt="" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$info['ugol_photo']?><!--"></div>-->
                                <a class="view-work"
                                   href="<?= Yii::getAlias('@web') .'/img/dom/' .$dom->id. '/cropped/sb_'.$avatar->path?>">
                                    <img class="thumbnail image-2" alt="foto"
                                         src="<?= Yii::getAlias('@web') .'/img/dom/' .$dom->id. '/cropped/p_'.$avatar->path?>">
                                </a>
                                <h3 class="hidden-print">ВНИМАНИЕ! АКЦИЯ!</h3>
                                <h4><?= $dom->address ?></h4>
                            </div>
                            <p><br><a class="btn big-link-1" href="javaScript:window.print();">
                                    <i class="glyphicon glyphicon-print"></i> Распечатать эту страницу</a></p>
                        </div>

                        <!--                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
                        <!--                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-title-more box2">-->
                        <!--                                <img class="image-responsive image-2" alt="акция" title="акция" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$info['card_photo']?><!--">-->
                        <!--                                <h2 class="text-center violet" id="blink2">--><?//=$info['text_card']?><!--</h2>-->
                        <!--                            </div>-->
                        <!---->
                        <!--                        </div>-->
                        <div class="work work-price-detail wow fadeInDown col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h3 class="text-center">Описание:</h3>
                            <div class="pricing-1-box-features">
                                <ul>
                                    <li>
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-home fa-2x color-price"></i></span></td>
                                                <td colspan="2"><b><?= $dom->area ?> м<sup>2</sup></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-picture-o fa-2x color-price"></i></span></td>
                                                <td><b><?= $dom->eart ?></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i class="fa fa-level-up fa-2x color-price"></i></span>
                                                </td>
                                                <td><b><?= $dom->poverh ?> этажа</b></td>
                                            </tr>

                                            <tr>
                                                <td><span class="pull-left"><i class="fa fa-clock-o fa-2x color-price"></i></span>
                                                </td>
                                                <td><b><?= $dom->readiness ?></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-map-marker fa-2x color-price"></i></span></td>
                                                <td><b><?= $dom->post_adress ?></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-info-circle fa-2x color-price"></i></span></td>
                                                <td><em class="font-more"><?= $dom->tech_data ?></em></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>




                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-print">
                        <h3 class="page-header">Дополнительные фотографии</h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 carousel slide media-carousel" id="media">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php $i = 0;
                                    $count = count($images->models);
                                    foreach ($images->models as $image):
                                    $i++; ?>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="bloc-carusel">
                                            <a class="view-work"
                                               href="<?= Yii::getAlias('@web') . '/img/dom/' . $image->idDom . '/' . $image->path ?>">
                                                <img class ="img-responsive" alt="foto dom" src="<?= Yii::getAlias('@web') . '/img/dom/' . $image->idDom . '/' . $image->path ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <?php if ($i % 4 == 0) : ?>

                                </div>

                            </div>
                            <?php endif; ?>
                            <?php if ($i % 4 == 0 && $i !== $count) : ?>
                            <div class="item">
                                <div class="row">
                                    <?php endif;
                                    ?>
                                    <?php endforeach; ?>
                                </div>
                                <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                                <a data-slide="next" href="#media" class="right carousel-control">›</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="container hidden-print">
            <div class="row">
                <?= $this->render('_callme', ['model' => $model]) ?>
            </div>
        </div>
        <p class="hidden-print"><br><br></p>

        <div class="visible-print-block">
            <img src="/img/promo/prazdnik.png">
            <div class="col-xs-12">

                <h3 class="violet">Специальное предложение для покупателей дома<br> до 14 февраля 2017 года.<br><br></h3>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-title-more box2">
                    <!--            <img class="img-responsive" style="width:200px;height:100px" alt="akciya" src="--><?//= Yii::getAlias('@web') .'/img/podarki.png '?><!--">-->
                    <h4 class="text-center violet">Приобритите этот прекрасный дом и получите получите праздничную скидку 20%!</h4>
                    <h3>Звоните прямо сейчас:&nbsp; +38 (067) 654 25 64</h3>
                </div>
                <div class="pricing-1-box-features">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="lead">П   </span><br>
                                        <span class="lead">Р   </span><br>
                                        <span class="lead">О   </span><br>
                                        <span class="lead">Д   </span><br>
                                        <span class="lead">А   </span><br>
                                        <span class="lead">Е   </span><br>
                                        <span class="lead">Т   </span><br>
                                        <span class="lead">С   </span><br>
                                        <span class="lead">Я   </span><br>
                                    </td>
                                    <td>
                                        <?php $files = DomImages::find()->where(['idDom' => $dom->id])->all(); ?>
                                        <p class="text-center"><img class="img-responsive" alt="foto" src="<?= Yii::getAlias('@web') .'/img/dom/' .$dom->id. '/cropped/p_'.$avatar->path?>"></p>
                                    </td>
                                    <td>
                                        <span class="lead">А</span><br>
                                        <span class="lead">К</span><br>
                                        <span class="lead">Ц</span><br>
                                        <span class="lead">И</span><br>
                                        <span class="lead">Я</span><br><br><br>
                                        <span class="lead">П</span><br>
                                        <span class="lead">О</span><br>
                                        <span class="lead">Д</span><br>
                                        <span class="lead">А</span><br>
                                        <span class="lead">Р</span><br>
                                        <span class="lead">О</span><br>
                                        <span class="lead">К</span><br>
                                    </td>
                                </tr>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <table class="table">
                                <h3 class="text-center"><?= $dom->address ?></h3>
                                <tbody>
                                <tr>
                                    <td><span class="pull-left"><i
                                                class="fa fa-info-circle fa-2x color-price"></i></span></td>
                                    <td><em class="font-more"><?= $dom->tech_data ?></em></td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php else :?>
    <section id="more">

        <div class="page-title-container hidden-print">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn">
                        <i class="fa fa-camera"></i>
                        <h1><?= $this->params['breadcrumbs'][] = $this->title; ?> /</h1>
                        <p>Ниже вы можете ознакомиться с подробной информацией</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container hidden-print">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 work wow fadeIn">
                    <div class="col-sm-12 testimonials-title-more wow fadeIn hidden-print">
                        <h2>Подробно про один из лучших домов в районе Совиньон:</h2>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="box1">
                                <?php $avatar = DomImages::findOne($dom->avatarId);
                                $files = DomImages::find()->where(['idDom' => $dom->id])->all(); ?>
                                <a class="view-work"
                                   href="<?= Yii::getAlias('@web') .'/img/dom/' .$dom->id. '/cropped/sb_'.$avatar->path?>">
                                    <img class="thumbnail image-2" alt="foto"
                                         src="<?= Yii::getAlias('@web') .'/img/dom/' .$dom->id. '/cropped/p_'.$avatar->path?>">
                                </a>
                                <h3 class="hidden-print"><?= $dom->price ?></h3>
                                <h4><?= $dom->address ?></h4>
                            </div>

                            <p><br><a class="btn big-link-1" href="javaScript:window.print();">
                                    <i class="glyphicon glyphicon-print"></i> Распечатать эту страницу</a>
                            </p>
                        </div>

                        <div class="work work-price-detail wow fadeInDown col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h3 class="text-center">Описание:</h3>
                            <div class="pricing-1-box-features">
                                <ul>
                                    <li>
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-home fa-2x color-price"></i></span></td>
                                                <td colspan="2"><b><?= $dom->area ?> м<sup>2</sup></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-picture-o fa-2x color-price"></i></span></td>
                                                <td><b><?= $dom->eart ?></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i class="fa fa-level-up fa-2x color-price"></i></span>
                                                </td>
                                                <td><b><?= $dom->poverh ?> этажа</b></td>
                                            </tr>

                                            <tr>
                                                <td><span class="pull-left"><i class="fa fa-clock-o fa-2x color-price"></i></span>
                                                </td>
                                                <td><b><?= $dom->readiness ?></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-map-marker fa-2x color-price"></i></span></td>
                                                <td><b><?= $dom->post_adress ?></b></td>
                                            </tr>
                                            <tr>
                                                <td><span class="pull-left"><i
                                                            class="fa fa-info-circle fa-2x color-price"></i></span></td>
                                                <td><em class="font-more"><?= $dom->tech_data ?></em></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-print">
                        <h3 class="page-header">Дополнительные фотографии</h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 carousel slide media-carousel" id="media">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php $i = 0;
                                    $count = count($images->models);
                                    foreach ($images->models as $image):
                                    $i++; ?>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="bloc-carusel">
                                            <a class="view-work"
                                               href="<?= Yii::getAlias('@web') . '/img/dom/' . $image->idDom . '/' . $image->path ?>">
                                                <img class ="img-responsive" alt="foto dom" src="<?= Yii::getAlias('@web') . '/img/dom/' . $image->idDom . '/' . $image->path ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <?php if ($i % 4 == 0) : ?>

                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if ($i % 4 == 0 && $i !== $count) : ?>
                            <div class="item">
                                <div class="row">
                                    <?php endif;
                                    ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                        <a data-slide="next" href="#media" class="right carousel-control">›</a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-address hidden-print hidden-sm-down">
                        <div class="map">
                            <?= $this->render('moremaps', ['lat' => $dom->geo_lat, 'lng' => $dom->geo_lng,]) ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-print">
                    <?= $this->render('_video', ['dom' => $dom]) ?>
                </div>
            </div>
        </div>



        <div class="container hidden-print">
            <div class="row">
                <?= $this->render('_callme', ['model' => $model]) ?>
            </div>
        </div>
        <p class="hidden-print"><br><br></p>

        <div class="visible-print-block">
            <img src="/img/logo.png">
            <div class="col-xs-12">

                <div class="pricing-1-box-features">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="lead">П   </span><br>
                                        <span class="lead">Р   </span><br>
                                        <span class="lead">О   </span><br>
                                        <span class="lead">Д   </span><br>
                                        <span class="lead">А   </span><br>
                                        <span class="lead">Е   </span><br>
                                        <span class="lead">Т   </span><br>
                                        <span class="lead">С   </span><br>
                                        <span class="lead">Я   </span><br>
                                    </td>
                                    <td>
                                        <?php $files = DomImages::find()->where(['idDom' => $dom->id])->all(); ?>
                                        <p class="text-center"> <img class="img-responsive" alt="foto" src="<?= Yii::getAlias('@web') .'/img/dom/' .$dom->id. '/cropped/p_'.$avatar->path?>"></p>
                                    </td>
                                </tr>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <table class="table">
                                <h3 class="text-center"><?= $dom->address ?></h3>
                                <tbody>
                                <tr>
                                    <td><span class="pull-left">
                                        <i class="fa fa-home fa-2x color-price"></i>
                                    </span>
                                    </td>
                                    <td colspan="2"><?= $dom->area ?> м<sup>2</sup></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left">
                                        <i class="fa fa-picture-o fa-2x color-price"></i>
                                    </span>
                                    </td>
                                    <td><?= $dom->eart ?></b></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left">
                                        <i class="fa fa-level-up fa-2x color-price"></i>
                                    </span>
                                    </td>
                                    <td><?= $dom->poverh ?> этажа</td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left">
                                        <i class="fa fa-clock-o fa-2x color-price"></i>
                                    </span>
                                    </td>
                                    <td><?= $dom->readiness ?></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left">
                                        <i class="fa fa-map-marker fa-2x color-price"></i>
                                    </span>
                                    </td>
                                    <td><?= $dom->post_adress ?></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left">
                                        <i class="fa fa-info-circle fa-2x color-price"></i>
                                    </span>
                                    </td>
                                    <td><em class="font-more"><?= $dom->tech_data ?></em></td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif;?>

<style>
    .carousel {
        margin-bottom: 0;
        padding: 0 40px 30px 40px;
    }
    /* The controlsy */
    .carousel-control {
        left: -12px;
        height: 40px;
        width: 40px;
        background: none repeat scroll 0 0 #222222;
        border: 4px solid #FFFFFF;
        border-radius: 23px 23px 23px 23px;
        margin-top: 90px;
    }
    .carousel-control.right {
        right: -12px;
    }
    /* The indicators */
    .carousel-indicators {
        right: 50%;
        top: auto;
        bottom: -10px;
        margin-right: -19px;
    }
    /* The colour of the indicators */
    .carousel-indicators li {
        background: #cecece;
    }
    .carousel-indicators .active {
        background: #428bca;
    }
</style>
