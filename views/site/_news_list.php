<?php

?>


<div class="col-md-4 col-sm-4">
    <a href="<?=\yii\helpers\Url::toRoute(['site/news-more','id'=> $model->id]) ?>">
        <img src="/img/news/<?= $model->id?>/cropped/<?= $model->avatar?>" class="img-responsive center-block img-padder" alt="<?= $model->title?>" title="<?= $model->title?>">
    </a>
</div>

<div class="col-md-8 col-sm-8">
    <h4 datetime="<?= date('d.m.Y', $model->datetime); ?>">
        <a href="<?=\yii\helpers\Url::toRoute(['site/news-more','id'=> $model->id]) ?>">
            <?= $model->title?>
        </a>
    </h4>
    <ul class="list-inline info">
        <li><i class="fa fa-user-md"></i> Администратор</li>
        <li><i class="fa fa-calendar"></i> <?= date('d.m.Y', $model->datetime); ?></li>
        <li><i class="fa fa-tag"></i> <a href="#"> <?= $model->category->title ?></a></li>
    </ul>

    <p>
        <?= Yii::$app->DLL->subStr($model->text, 400) ?>
    </p>
    <p>
        <a href="<?=\yii\helpers\Url::toRoute(['site/news-more','id'=> $model->id]) ?>" class="btn big-link-1">Продолжить чтение</a>
    </p>

</div>
