<?php
use yii\helpers\Html;

$this->registerMetaTag(['name' => 'keywords', 'content' => $news->title]);
$this->registerMetaTag(['name' => 'description', 'content' => $news->title], 'description');


$this->title = 'БЛОГ - ' . $news->title;

$img_fb = 'http://domsovinyon.com/img/news/'.$news->id.'/cropped/'.$news->avatar;
$url_facebook = 'http://domsovinyon.com/site/news-more?id='.$news->id;
//$title=urlencode($news->title);
$title=$news->title;
$url=$url_facebook;
$summary=$news->summary_facebook;
$image_fb=$img_fb;

$this->registerMetaTag(['property'=>'og:title', 'content'=>$title], 'og:title');
$this->registerMetaTag(['property'=>'og:type', 'content'=>'article'], 'og:type');
$this->registerMetaTag(['property'=>'og:url', 'content'=>$url], 'og:url');
$this->registerMetaTag(['property'=>'og:image', 'content'=>$image_fb], 'og:image');
$this->registerMetaTag(['property'=>'og:site_name', 'content'=>'domsovinyon.com'], 'og:site_name');
$this->registerMetaTag(['property'=>'og:admins', 'content'=>'Dom Sovinyon']);
$this->registerMetaTag(['property'=>'og:description', 'content'=>$news->title], 'og:description');




?>
<section id="news-more">
    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn">
                    <i class="fa fa-book"></i>
                    <h1><?=$this->params['breadcrumbs'][] = $this->title;?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="presentation-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInLeftBig box2">
                    <h1><span class="violet">DOM SOVINYON </span>, предлагаем красивые дома в лушем районе любимой Одессы.</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class= "blog-content">
    <div class= "container">
        <div class= "row box2">
            <main class="blog1col col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="about-us-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 wow fadeIn">
                                <div class="col-sm-12 testimonials-title-more wow fadeIn">
                                    <h2><?= $news->title?></h2>
                                </div>
                                <div class="row">
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 wow fadeIn size-text text-left">
                                        <div class="col-sm-4 wow fadeIn size-text">
                                            <img class="thumbnail not-hover" alt="foto" src="/img/news/<?= $news->id?>/cropped/<?= $news->avatar?>">
                                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                                            <script src="//yastatic.net/share2/share.js"></script>
                                            <div class="ya-share2" data-services="viber,whatsapp,vkontakte,facebook,twitter,odnoklassniki" data-size="m"></div>
                                        </div>
                                        <?= $news->text?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="widget widget_categories">
                    <h4><span>Категории</span></h4>
                    <ul>
                        <?php foreach($category as $cat):  ?>

                            <li class="cat-item cat-item-4">
                                <?= Html::a($cat->title , 'index?NewsSearch%5Bcategory_id%5D='.$cat->id.'&r=site%2Fnews') ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="widget widget_iw_last_posts">
                    <h4><span>Интересные <br>статьи и изображения:</span></h4>
                    <article>

                        <?= \yii\widgets\ListView::widget([
                            'dataProvider' => $dataProviderLast,
                            'itemView' => '_news_last',
                            'options' => ['class' => 'archive'],
                            'emptyText' => 'Список пуст',
                            'layout' => "{items}<div class=\"clear\"></div>",
                            'itemOptions' => [
                                'tag' => 'div',
                                'class' => 'row',
                            ],
                        ]) ?>
                    </article>
                </div>
                <div class="widget widget_tag_cloud">
                    <h4><span>Тэги</span></h4>
                    <div class="tagcloud">
                        <?php foreach($dataProviderTag->models as $tag):  ?>

                            <?= Html::a($tag->tag_news , 'index?NewsSearch%5Bcategory_id%5D='.$tag->category_id.'&r=site%2Fnews') ?>

                        <?php endforeach; ?>
                    </div>
                </div>
            </aside>
        </div>
        <?= $this->render('_callme', ['model' => $model]) ?>
    </div>
    </div>
</section>


