<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Таблица управления Блогом';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="news-index">

            <div class="box">

                <div class="box-header with-border">
                    <div class="pull-left">
                        <?= Html::a('Создать Новость', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <div class="pull-right">
                        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                </div>

                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'rowOptions' => ['height' => '150'],
                        'columns' => [
                    /*        [
                                'class' => 'yii\grid\SerialColumn'
                            ],*/
                          /*  [
                                'attribute' => 'avatar',
                                'format' => 'html',
                                'label' => 'Фото',
                                'value' => function ($data) {
                                    return '<img class="img-responsive" src=' . Yii::getAlias('@web') . '/img/dom/news/' . $data->id . '/cropped/' . $data->avatar . '>';
                                }
                            ],*/
//                            'id',
//                            'text:ntext',
                            [
                                'attribute' => 'text',
                                'value' => function($data) {
                                    return Yii::$app->DLL->subStr($data->text, 600);
                                }
                            ],
                            [
                                'attribute' => 'datetime',
                                'value' => function($data){
                                    return Yii::$app->formatter->asDatetime($data->datetime, "php:m/d/Y ");
                                }
                            ],
                            [
                                'attribute' => 'status_id',
                                'value' => function($data){
                                    switch ($data->status_id){
                                        case 1: return 'Черновик';
                                        case 2: return 'Опубликовано';
                                        default: return '';
                                    }

                                }
                            ],
                            'title:ntext',
                            'category.title',
                            [
                                'attribute' => 'active_facebook',
                                'value' => function($data){
                                    switch ($data->status_id){
                                        case 1: return 'Отключено';
                                        case 2: return 'Подключена';
                                        default: return '';
                                    }

                                }
                            ],
                            // 'avatar',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['width' => '70'],
                                'template' => '{view} {update} {delete} {link}',
                            ],
                        ],
                    ]); ?>
                </div>

            </div>
        </div>
    </div>
</div>

<style>
    .table.table-striped.table-bordered > tr{
        max-height:300px;
        overflow:scroll;
    }
</style>