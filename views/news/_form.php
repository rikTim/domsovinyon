<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\widgets\MaskedInput;
use vova07\imperavi\Widget;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="news-form">

                <div class="box box-default">

                    <div class="box-body">

                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <div class="form-group field-news-datetime">
                                    <label class="control-label" for="needies-datetime">Дата</label>

                                    <?php if (!empty($model->datetime)) {
                                        $model->datetime = Yii::$app->formatter->asDatetime($model->datetime, "php:m/d/Y ");
                                    } else {
                                        $model->datetime = Yii::$app->formatter->asDatetime(time(), "php:m/d/Y ");
                                    } ?>
                                    <?php
                                    echo MaskedInput::widget([
                                        'model' => $model,
                                        'attribute' => 'datetime',
                                        'name' => 'datetime',
                                        'mask' => '99/99/9999'
                                    ]);
                                    ?>
                                    <div class="help-block"></div>
                                </div>
                                <? /*= $form->field($model, 'status_id')->textInput(['maxlength' => true]) */ ?>
                                <?= $form->field($model, 'category_id')->dropDownList($category, ['prompt' => 'Выберите категорию']) ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <?= $form->field($model, 'status_id')->dropDownList(['prompt' => 'Выберите статус', '1' => 'Черновик', '2' => 'Опубликовано']) ?>
                                <?= $form->field($model, 'title')->textarea(['rows' => 1]) ?>
                            </div>
                            <!--                            --><?//= $form->field($model, 'text')->widget(Widget::className(), [
                            //                                'settings' => [
                            //                                    'lang' => 'ru',
                            //                                    'minHeight' => 200,
                            //                                    'plugins' => [
                            //                                        'clips',
                            //                                        'fullscreen',
                            //                                        'imagemanager',
                            //                                    ],
                            //                                     'imageUpload' => Url::to(['/img/news']),
                            //                                     'imageManagerJson' => Url::to(['/img/news'])
                            //
                            //                                ],
                            //                               // 'imageUpload' => Url::to(['/img/news']),
                            //                               // 'imageManagerJson' => Url::to(['/img/news'])
                            //                            ]);
                            //                            ?>
                            <?= $form->field($model, 'text')->widget(\yii\redactor\widgets\Redactor::className(), [
                                'clientOptions' => [
                                    'imageManagerJson' => ['/redactor/upload/image-json'],
                                    'imageUpload' => ['/redactor/upload/image'],
                                    'fileUpload' => ['/redactor/upload/file'],
                                    'lang' => 'ru',
                                    'plugins' => ['clips', 'fontcolor','imagemanager']
                                ]
                            ])?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                                <h4 class="text-center">Изображение на главной странице Блога</h4>
                                <div class="col-md-3 col-sm-3">
                                <?= $form->field($upload, 'imageFile')->fileInput() ?>
                                    </div>
                                <div class="col-md-3 col-sm-3">
                                    <img src="/img/news/<?= $model->id?>/cropped/<?= $model->avatar?>" class="img-thumbnail size" alt="">
                                </div><br>
                            </div>
                            <!--                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                            <!--                                <h4 class="text-center">Данные для кнопки FaceBook</h4>-->
                            <!--                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">-->
                            <!--                                    --><?//= $form->field($model, 'summary_facebook')->textarea(['rows' => 1]) ?>
                            <!--                                </div>-->
                            <!--                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">-->
                            <!--                                    --><?//= $form->field($model, 'active_facebook')->dropDownList(['prompt' => 'Выберите статус', '1' => 'Отключено', '2' => 'Подключена']) ?>
                            <!--                                </div>-->
                            <!--                            </div>-->
                        </div>

                        <div class="box-footer">
                            <div class="form-btn">
                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
