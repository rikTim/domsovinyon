<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'View News' . ' #' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <div class="box">

        <div class="box-header with-border">
            <?= Html::a('Управление', ['index'], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

        </div>

        <div class="box-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'text:ntext',
                    'datetime',
                    'status_id',
                    'active_facebook',
                    'title:ntext',
                    'category_id:ntext',
//                    'avatar',
                ],
            ]) ?>

        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="col-md-10 col-sm-10 col-offset-sm-1 box1">
                <div class="col-md-3 col-sm-3">
                    <img src="/img/news/<?= $model->id?>/cropped/<?= $model->avatar?>" class="img-thumbnail size" alt="">
                </div>

                <div class="col-md-7 col-sm-7">
                    <h4 datetime="<?= date('d.m.Y', $model->datetime); ?>">
                        <?= $model->title?>
                    </h4>
                    <ul class="list-inline info">
                        <li><i class="fa fa-user-md"></i> Администратор</li>
                        <li><i class="fa fa-calendar"></i> <?= date('d.m.Y', $model->datetime); ?></li>
                        <li><i class="fa fa-tag"></i> <a href="#"> <?= $model->category->title ?></a></li>
                    </ul>

                    <p>
                        <?= Yii::$app->DLL->subStr($model->text, 400) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<section class= "blog-content">
    <div class= "container">
        <div class= "row">
            <main class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-sm-ofset-1">
                                <div class="col-sm-12">
                                    <h2><?= $model->title?></h2>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 wow fadeIn size-text">
                                        <div class="text-left">
                                            <div class="col-sm-4 wow fadeIn size-text">
                                                <img class="img-thumbnail not-hover" alt="foto" src="/img/news/<?= $model->id?>/cropped/<?= $model->avatar?>">
                                            </div>
                                            <?= $model->text?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div
                    </div>
                </div>
            </main>
        </div>
    </div>
</section>


<style>
    .box1 {
        padding:10px;
        /*  margin:20px auto;*/
        border: 1px solid rgb(200, 200, 200);
        box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
        background:  #f8f8f8;
        border-radius: 4px;
    }
    .size{
        max-height: 250px;
        max-width: 200px;
    }


</style>