<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Dom;
use app\models\Promo;

$dom=Dom::find()->all();

/* @var $this yii\web\View */
/* @var $searchModel app\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Создание Акционного предложения';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="promo-index">

    <div class="box">

        <div class="box-header with-border">
            <h1><?= $this->params['breadcrumbs'][] = $this->title; ?>
                <?php $status=$info['status'];
                $status_id= $status==1?'<i class="success" style="color:green">ОПУБЛИКОВАНО</i>':'<i style="color:cornflowerblue;">ЧЕРНОВИК</i>';?>
                <?=$status_id?></h1>
            <!--            <div class="pull-left">-->
            <!--                --><?//= Html::a('Create Promo', ['create'], ['class' => 'btn btn-success']) ?>
            <!--            </div>-->
            <!--            <div class="pull-right">-->
            <!--                --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>
            <!--            </div>-->
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'promo_textUP',
                    'promo_textDown',
                    'text_card',
                    'dop_info',
                    [
                        'attribute' => 'id_dom',
                        'value' => function($data){
                            $id_adress=$data->id_dom;
                            $dom = Dom::find()->where(['id'=> $id_adress])->all();
                            foreach ($dom as $adress){
                                return $adress['address'];
                            }
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function($data){
                            switch ($data->status){
                                case 0: return 'Черновик';
                                case 1: return 'Опубликовано';
                                default: return '';
                            }

                        }
                    ],
                    [
                        'attribute' => 'slider_home',
                        'value' => function($data){
                            switch ($data->status){
                                case 0: return 'Нет';
                                case 1: return 'Включить';
                                default: return '';
                            }

                        }
                    ],
                    // 'promo_date',
                    // 'promo_photo',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view} {update} {link}',
                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->
<!---->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <h3 class="text-center"><h3 class="text-center">Блок на странице "Главная"</h3>-->
<!--                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box2">-->
<!--                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--                        <h2 class="violet text-center" style="font-size: 24px;"><span class="violet text-uppercase" id="blink1">--><?//=$info['promo_textUP'] ?><!--</span></h2>-->
<!--                    </div>-->
<!--                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--                        <div class="col-sm-3">-->
<!--                            <div class="lenta-action1"><img class="image-2 img-responsive" alt="" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$info['ugol_photo']?><!--"></div>-->
<!--                            <img class="image-3" alt="" src="--><?//= Yii::getAlias('@web') .'/img/promo/'. $info['promo_photo'] ?><!--">-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-sm-9">-->
<!--                            <div class="row"><br>-->
<!--                                <h3 class="text-center" style="margin:2em;"> Здесь будет счетчик.<br><span class="violet">Время до --><?//=$info['promo_date'] ?><!--</span></h3>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--                            <h2 class="text-uppercase text-center violet" style="font-size: 24px;">--><?//=$info['promo_textDown'] ?><!--</h2>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="work-bottom text-center">-->
<!--                        <a class="btn-sm big-link-1" href="">УЗНАТЬ БОЛЬШЕ</a>-->
<!--                        <br><br>-->
<!--                    </div>-->
<!--                </div>-->
<!--        </div>-->
<!--        <div class="row"><h3 class="text-center">Блок на странице "Подробнее"</h3></div>-->
<!--        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box2">-->
<!--            <h5 class="text-center">Отображаемый текст вверху,перед блоками:</h5>-->
<!--            <h4 class="text-center violet"><strong>--><?//= $info['text_more']?><!--</strong></h4>-->
<!--            </div>-->
<!--            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br>-->
<!--            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sm-offset-3">-->
<!--                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-title-more box2 text-center">-->
<!--                    <img style="width:300px;height:200px;" class="image-responsive image-2" alt="акция" title="акция" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$info['card_photo']?><!--">-->
<!--                    <h2 class="text-center violet" id="blink2">--><?//=$info['text_card']?><!--</h2>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!---->
<!--        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!---->
<!--            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
<!--                <h4 class="text-center">Изображение подложки страницы "Подробнее"</h4>-->
<!--                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 testimonials-title-more box2 text-center">-->
<!--                    <img style="width:82px;height:82px;" class="image-responsive image-2" alt="" title="" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$info['bground_img']?><!--">-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
<!--                <h4 class="text-center">Изображение уголок с боку на фото дома.</h4>-->
<!--                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 testimonials-title-more box2 text-center">-->
<!--                    <img style="width:82px;height:82px;" class="image-responsive image-2" alt="" title="" src="--><?//= Yii::getAlias('@web') .'/img/promo/'.$info['ugol_photo']?><!--">-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--<style type="text/css">-->
<!--    .box2 {-->
<!--        position:relative;-->
<!--        width:100%;-->
<!--        padding:5px;-->
<!--        border: 1px solid rgb(200, 200, 200);-->
<!--        box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;-->
<!--        background: #ffffff;-->
<!--        border-radius: 4px;-->
<!--    }-->
<!--    .violet {-->
<!--        color: #bf5181;-->
<!--    }-->
<!--    #blink1 {-->
<!--        -webkit-animation: blink1 3s linear infinite;-->
<!--        animation: blink1 3s linear infinite;-->
<!--    }-->
<!--    @-webkit-keyframes blink1 {-->
<!--        0% { color: rgba(203, 85, 138,1); }-->
<!--        50% { color: rgba(203, 85, 138, 0); }-->
<!--        100% { color: rgba(203, 85, 138,1); }-->
<!--    }-->
<!--    @keyframes blink1 {-->
<!--        0% { color: rgba(203, 85, 138, 1); }-->
<!--        50% { color: rgba(203, 85, 138, 0); }-->
<!--        100% { color: rgba(203, 85, 138, 1); }-->
<!--    }-->
<!--    .image-3 {-->
<!--        width:100%;-->
<!--        height:230px;-->
<!--        -webkit-box-shadow:0 1px 1px rgba(0,0,0,0.2);-->
<!--        -moz-box-shadow:0 1px 1px rgba(0,0,0,0.2);-->
<!--        box-shadow:0 5px 5px 5px rgba(0,0,0,0.2);-->
<!--    }-->
<!--    .lenta-action1 {-->
<!--        width: 85px;-->
<!--        height: 88px;-->
<!--        overflow: hidden;-->
<!--        position: absolute;-->
<!--        top: -2px;-->
<!--        right: 8px;-->
<!--    }-->
<!---->
<!--    .image-2:hover {-->
<!--        cursor: pointer;-->
<!--        opacity: 1;-->
<!--        z-index: 5;-->
<!--        -moz-transform: scale(1.05);-->
<!--        -webkit-transform: scale(1.05);-->
<!--        -o-transform: scale(1.05);-->
<!--        -ms-transform: scale(1.05);-->
<!--        transform: scale(1.05);-->
<!--    }-->
<!--    a.big-link-1 {-->
<!--        display: inline-block;-->
<!--        padding: 5px 22px;-->
<!--        background: #bf5181;-->
<!--        color: #fff;-->
<!--        font-style: italic;-->
<!--        text-decoration: none;-->
<!--        -khtml-border-radius: 3px;-->
<!--        -webkit-border-radius: 3px;-->
<!--        -moz-border-radius: 3px;-->
<!--        border-radius: 3px;-->
<!--        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);-->
<!---->
<!--    }-->
<!---->
<!--    a.big-link-1:hover {-->
<!--        color: #ffffff;-->
<!--        background-color: #d75b91;-->
<!--        -webkit-transition: .5s;-->
<!--        -moz-transition: .5s;-->
<!--        -o-transition: .5s;-->
<!--        transition: .5s;-->
<!--    }-->
<!---->
<!--    a.big-link-1:active {-->
<!--        -moz-box-shadow: 0 5px 10px 0 rgba(0,0,0,.15) inset, 0 -1px 25px 0 rgba(0,0,0,.05) inset;-->
<!--        -webkit-box-shadow: 0 5px 10px 0 rgba(0,0,0,.15) inset, 0 -1px 25px 0 rgba(0,0,0,.05) inset;-->
<!--        box-shadow: 0 5px 10px 0 rgba(0,0,0,.15) inset, 0 -1px 25px 0 rgba(0,0,0,.05) inset;-->
<!--    }-->
<!--    .testimonials-title-more h2 {-->
<!--        margin: 10px auto;-->
<!--        font-family: 'Lobster', cursive;-->
<!--        font-size: 18px;-->
<!--        color: #d75b91;-->
<!--        font-weight: bold;-->
<!--    }-->
<!--</style>-->