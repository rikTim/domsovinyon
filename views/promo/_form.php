<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Dom;

/* @var $this yii\web\View */
/* @var $model app\models\Promo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-form">

	<div class="box box-default">

		<div class="box-body">

			<?php $form = ActiveForm::begin(); ?>

			<?= $form->field($model, 'promo_textUP')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'promo_textDown')->textInput(['maxlength' => true]) ?>
<!--			--><?//= $form->field($model, 'text_card')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'text_more')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'dop_info')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'id_dom')->dropDownList($id_dom, ['prompt' => 'Адрес дома']) ?>

			<?= $form->field($model, 'status')->dropDownList(['prompt' => 'Выберите статус', '0' => 'Черновик', '1' => 'Опубликовано']) ?>
			<?= $form->field($model, 'slider_home')->dropDownList(['prompt' => 'Отображать слайдер', '0' => 'НЕТ', '1' => 'Да']) ?>
<!--			<p>Формат даты(2017,02,14)</p>-->
<!--			--><?//= $form->field($model, 'promo_date')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'promo_photo')->textInput(['maxlength' => true]) ?>
<!--			--><?//= $form->field($model, 'card_photo')->textInput(['maxlength' => true]) ?>
<!--			--><?//= $form->field($model, 'ugol_photo')->textInput(['maxlength' => true]) ?>
<!--			--><?//= $form->field($model, 'bground_img')->textInput(['maxlength' => true]) ?>

		</div>

		<div class="box-footer">
			<div class="form-btn">
				<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		</div>

		<?php ActiveForm::end(); ?>

	</div>

</div>
