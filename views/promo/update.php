<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Promo */

$this->title = 'Update Promo' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promo-update">

    <?= $this->render('_form', [
        'model' => $model,
        'id_dom'=>$id_dom,
    ]) ?>

</div>
