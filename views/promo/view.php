<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Promo */

$this->title = 'View Promo' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-view">

    <div class="box">

        <div class="box-header with-border">
        <?= Html::a('Далее', ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>

        </div>

        <div class="box-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'promo_textUP',
                'promo_textDown',
                'text_card',
                'dop_info',
            'id_dom',
            'status',
            'promo_date',
            'promo_photo',
            ],
        ]) ?>

        </div>

    </div>

</div>
