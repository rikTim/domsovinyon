<?php
use app\models\Dom;
use yii\helpers\Url;

?>
<footer class="hidden-print">
    <div class="container">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 footer-box wow fadeInUp">
                <h4>О нас</h4>
                <div class="footer-box-text">
                    <p>
                        Во все кризисные годы наша компания остается верна своему принципу - строить быстрее, чтобы сократить сроки ответственности перед инвесторами.
                        Качество строительства при этом всегда остается неизменно высоким. </p>
                    <p><a href="<?= Url::toRoute('site/about')?>">Читать далее...</a></p>
                </div>
            </div>
            <?php $models = Dom::find()->all(); ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 footer-box wow fadeInUp">
                <h4>Фото наших проектов</h4>
                <div class="footer-box-text flickr-feed">
                    <?php foreach($models as $model):?>
                        <a href="<?= Url::toRoute(['site/more','id'=>$model->id])?>">
                            <img alt="Фото дома" src="<?= Yii::getAlias('@web') .'/img/dom/'.$model->id .'/cropped/' . $model->avatar->path?>">
                        </a>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 footer-box wow fadeInDown">
                <h4>СВЯЖИТЕСЬ С НАМИ</h4>
                <div class="footer-box-text footer-box-text-contact">
                    <p><i class="fa fa-phone"></i> Телефон: +38 (067) 654 25 64</p>
                    <p><a href="mailto:sales@domsovinyon.com"><i class="fa fa-envelope"></i> Email:sales@domsovinyon.com</a></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn">
                    <div class="footer-border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 footer-copyright wow fadeIn">
                    <p>&copy; DOM SOVINYON <?= date('Y')?> - All rights reserved.</p>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 footer-social wow fadeIn">
                    <a href="https://m.facebook.com/domsovinyon/"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/dom.sovinyon/"><i class="fa fa-instagram"></i></a>
                </div>
                <a href="#"  class="signup" id="popup_toggle" onclick="return false;"><div class="circlephone" style="transform-origin: center;"></div>
                    <div class="circle-fill" style="transform-origin: center;"></div>
                    <div class="img-circle" style="transform-origin: center;">
                        <div class="img-circleblock" style="transform-origin: center;"><span class="text-mini"></span></div>
                    </div>
                </a>
            </div>

    </div>
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>



    <?php \yii\bootstrap\Modal::begin([
        'id' => 'my-modal',
        'header' => '<h4> Заказать обратный звонок</h4>',
        'size' => 'modal-md',

    ]);
    echo "<div id='call-me-body'><i class='fa fa-mobile-phone fa-5x'>&nbsp;</i>
<i class='fa fa-exchange fa-4x'>&nbsp;</i><i class='fa fa-mobile-phone fa-5x'></i>
<h5 class='text-uppercase'>Оставьте ваши контактные данные и наш консультант свяжется с вами</h5></div>";
    \yii\bootstrap\Modal::end(); ?>

    <?php \yii\bootstrap\Modal::begin([
        'id' => 'call-me-success',
        'header' => '<h4 class="price-color"> Ваш запрос успешно отправлен.</h4>',
        'size' => 'modal-sm',

    ]);?>
    <?php \yii\bootstrap\Modal::end(); ?>
</footer>