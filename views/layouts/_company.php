<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container">
    <div class="row">
        <div class="clearfix"></div>
        <div class="presentation-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInLeftBig box2">
                        <h2><span class="violet">DOM SOVINYON </span> - доверие основанное на качестве и репутации</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="services-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="service wow fadeInUp">
                                <div class="service-icon"><i class="fa fa-lightbulb-o icon-lightbulb color-price"></i></div>
                                <h3 class="size-company">Эксклюзивные проекты</h3>
                                <p>Помощь в разработке индивидуального проекта вашего будущего дома.</p>
                                <a class="big-link-1" href="<?=Url::toRoute(['site/project']) ?>">Подробнее</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="service wow fadeInUp">
                                <div class="service-icon"><i class="fa fa-magic color-price"></i></div>
                                <h3 class="size-company">Передовые технологии</h3>
                                <p>Внедрение передовых энергоэффективных технологий в строительство.</p>
                                <a class="big-link-1" href="<?=Url::toRoute(['site/tehnology']) ?>">Подробнее</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="service wow fadeInDown">
                                <div class="service-icon"><i class="fa fa-home color-price"></i></div>
                                <h3 class="size-company">Лучшие материалы</h3>
                                <p>Использование сертифицированных экологически чистых строительных материалов.</p>
                                <a class="big-link-1" href="<?=Url::toRoute(['site/material']) ?>">Подробнее</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="service wow fadeInDown">
                                <div class="service-icon"><i class="fa fa-leanpub color-price"></i></div>
                                <h3 class="size-company">Наличие документов</h3>
                                <p>Строительство на основании разрешительных документов на проверенных земельных участках.</p>
                                <a class="big-link-1" href="<?=Url::toRoute(['site/documenty']) ?>">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>