<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\themes\adminLTE\components\ThemeNav;

?>
<?php $this->beginContent('@app/themes/adminLTE/layouts/main.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/user_accounts.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        <?php
                        $info[] = Yii::t('app','Здравствуйте');

                        if(!Yii::$app->user->isGuest) {
                            $user = \app\models\User::findOne(Yii::$app->user->id);
                            $info[] = ucfirst($user->username);
                        }

                        echo implode(', ', $info);

                        ?>
                    </p>
                    <a><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php
            echo Menu::widget([
                'encodeLabels'=>false,
                'options' => [
                    'class' => 'sidebar-menu'
                ],
                'items' => [
                    ['label'=>Yii::t('app','Панель навигации'), 'options'=>['class'=>'header']],
                    ['label' => ThemeNav::link('ГЛАВНАЯ-DOM', 'fa fa-home'), 'url' => ['dom/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Слайдер', 'fa fa-image'), 'url' => ['slider/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Виртуал Тур', 'fa fa-image'), 'url' => ['panorama/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('БЛОГ', 'fa fa-book'), 'url' => ['news/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('БЛОГ-Теги', 'fa fa-book'), 'url' => ['tag/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Сообщения', 'fa fa-user'), 'url' => ['user-info/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Видео', 'fa fa-video-camera'), 'url' => ['video/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Акция', 'fa fa-diamond'), 'url' => ['promo/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                ],
            ]);
            ?>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => false,
            ]) ?>
        </section>


        <!-- Main content -->
        <section class="content" style="margin-top: 20px">
            <?php echo $content; ?>
        </section><!-- /.content -->

    </div><!-- /.right-side -->
<?php $this->endContent();