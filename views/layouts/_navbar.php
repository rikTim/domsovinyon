<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;



NavBar::begin([
//    'brandLabel' => 'DOM SOVINYON',
    'brandLabel' => Html::img('/img/logo.png', ['alt'=>'Logo DOM SOVINYON']),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-fixed-top nav-height meandr',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right',
        'id' => 'top-navbar-1',
    ],
    'items' => [
        ['label' => '<i class="fa fa-home"></i><br>Главная', 'url' => ['site/index']],
        ['label' => '<i class="fa fa-user"></i><br>О нас', 'url' => ['site/about']],
//       ['label' => '<i class="glyphicon glyphicon-home"></i><br>Услуги', 'url' => ['/site/services']],
        ['label' => '<i class="fa fa-book"></i><br>Блог', 'url' => ['site/news']],
        ['label' => '<i class="fa fa-envelope"></i><br>Контакты', 'url' => ['site/contact']],
//       Yii::$app->user->isGuest ? // Если пользователь гость, показыаем ссылку "Вход", если он авторизовался "Выход"
//         ['label' => 'Вход', 'url' => ['/site/login']] :
//          [
//             'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
//              'url' => ['/site/logout'],
//             'linkOptions' => ['data-method' => 'post']
//           ],
    ],
   'encodeLabels' =>false,
]);
NavBar::end();

?>