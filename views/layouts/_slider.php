<?php

?>

<div class="container fade-carousel">
    <div id="carousel" class="carousel slide carousel-fade effect5" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
            <li data-target="#carousel" data-slide-to="4"></li>
        </ol>

        <div class="carousel-inner carousel-zoom">
            <?php $sliders = \app\models\SliderMain::find()->all();?>
            <?php $count = 0; foreach($sliders as $slider) :?>
                <?php
                $class = '';
                if($count < 1){
                    $class = 'active';
                }else $class = '';
                $count++; ?>

                <div class="<?php echo $class ?> item">
                    <img class="img-responsive" alt="Главный слайдер" src="<?= Yii::getAlias('@web') . '/img/slider' . $slider->avatar->idDom . '/cropped/sb_' . $slider->avatar->path ?>">

                </div>
            <?php endforeach;?>
        </div>

        <a class="carousel-control left" href="#carousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left lslide"></span></a>
        <a class="carousel-control right" href="#carousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>

</div>
