<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

	<div class="box box-default">

		<div class="box-body">

	    <?php $form = ActiveForm::begin(); ?>

<!--	    --><?//= $form->field($model, 'id')->textInput() ?>

		<?= $form->field($model, 'idDom')->dropDownList($idDom, ['prompt' => 'Адрес дома']) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_video')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'status_id')->dropDownList(['prompt' => 'Выберите статус', '1' => 'Черновик', '2' => 'Опубликовано']) ?>

		</div>

	    <div class="box-footer">
			<div class="form-btn">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    	</div>
	    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
