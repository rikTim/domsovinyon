<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Create Video', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'idDom',
            'link',
            'name_video',
    //        'status_id',
            [
                'attribute' => 'active',
                'value' => function($data){
                    switch ($data->status_id){
                        case 1: return 'Черновик';
                        case 2: return 'Опубликовано';
                        default: return '';
                    }

                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
