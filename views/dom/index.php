<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DomAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Администратор Domsovinyon';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dom-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить дом', ['create'], ['class' => 'btn btn-success']) ?>
            </div>

        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'avatar_id',
                        'format' => 'html',
                        'label' => 'Фото',
                        'value' => function ($data) {
                            return '<img class="img-responsive" src=' . Yii::getAlias('@web') . '/img/dom/' . $data->id . '/cropped/f_' . $data->avatar->path . ' alt="foto house">';
                        }
                    ],
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
                    'tech_data',
//            'text:ntext',
//                    'avatarId',
                    'price',

                    [
                        'attribute' => 'active',
                        'value' => function($data){
                            switch ($data->active){
                                case 1: return 'Активна';
                                case 2: return 'Черновик';
                                default: return '';
                            }

                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view} {update}  {delete}{image}',


                        'buttons' => [
                            'image' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-picture"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch($action){

                                case 'update':
                                    $url = \yii\helpers\Url::toRoute(['dom/update','id'=>$model->id]);
                                    return $url;
                                case 'view':
                                    $url = \yii\helpers\Url::toRoute(['dom/view','id'=>$model->id]);
                                    return $url;
                                case 'delete':
                                    $url = \yii\helpers\Url::toRoute(['dom/delete','id'=>$model->id]);
                                    return $url;
                                case 'image':
                                    $url = \yii\helpers\Url::toRoute(['dom/imagemore','id'=>$model->id]);
                                    return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
