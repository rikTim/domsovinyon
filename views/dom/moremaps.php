<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;

$coord = new LatLng(['lat' => $lat, 'lng' => $lng]);
$map = new Map([
    'center' => $coord,
    'zoom' => 19,
    'width' => '100%',
    'height' => '100%',
]);

// Позволяет добавить маркер в настоящее время
$marker = new Marker([
    'position' => $coord,
    'title' => 'Офис продаж',
]);

// Обеспечить общий InfoWindow к маркеру
$marker->attachInfoWindow(
    new InfoWindow([
        'content' => '<p>Отличный дом для добрых людей!</p>'
    ])
);

// Добавить маркер на карте
$map->addOverlay($marker);

// Отображение карты
echo $map->display();