<?php


/* @var $this yii\web\View */
/* @var $model app\models\Dom */

$this->title = 'Редактировать Dom' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dom', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="container">
    <div class="row">
        <div class="dom-update col-sm-12">
            <div class="col-sm-11 col-sm-ofset-1">
                <div class="alert alert-info">Информация про ДОМ</div>
                <?= $this->render('_form', [
                    'model' => $model,
                    'upload' =>$upload,
                ]) ?>
            </div>
        </div>
    </div>
    </div>