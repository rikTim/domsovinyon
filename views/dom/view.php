<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\DomImages;

/* @var $this yii\web\View */
/* @var $model app\models\Dom */

$this->title = 'View Dom' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Doms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="dom-view">
                    <div class="box">
                        <div class="box-header with-border">
                            <?= Html::a('Управление таблицей', ['index'], ['class' => 'btn btn-warning']) ?>
                            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
                            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                        <?php
                  /*      switch ($model->active){
                        case 1: $model->active= 'Активна';
                        case 2: $model->active= 'Черновик';
                        default: $model->active='' ;
                        }*/
                        if ($model->active==1) {
                            $model->active = 'Активна';
                        }else{
                            $model->active= 'Черновик';
                        }
                        if ($model->sales==1) {
                            $model->sales = 'SALES-продается';
                        }else{
                            $model->sales= 'ПРОДАНО';
                        }
                        ?>

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'tech_data',
                                'text:ntext',
                                'avatarId',
                                'price',
                                'material_house',
                                'material_roof',
                                'material_windows',
                                'material_doors',
                                'material_wall',
                                'area',
                                'eart',
                                'address',
                                'dop_info',
                                'geo_lat',
                                'geo_lng',
                                'post_adress',
                                'readiness',
                                'poverh',
                                'active',
                                'sales',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        <div class="col-sm-4">
            <div class="alert alert-info">Этикетка ДОМ опубликовання на сайте</div>
            <div class="work">
                <div class="box1">
                    <a class="view-work" href="<?= Yii::getAlias('@web') .'/img/dom/' .$model->id .'/cropped/p_'.$model->avatar->path?>">
                        <img class="image-2" alt="<?= $model->address?>" src="<?= Yii::getAlias('@web') .'/img/dom/' .$model->id .'/cropped/'.$model->avatar->path?>"></a>
                    <h3><?= $model->price?></h3>
                    <h4><?= $model->address?></h4>
                </div>
                <div class="pricing-1-box-features">
                    <ul>
                        <li><span class = "pull-left"><i class = "glyphicon glyphicon-home color-price"></i></span><b><?= $model->area?> м<sup>2</sup></b></li>
                        <li><span class = "pull-left"><i class = "glyphicon glyphicon-picture color-price"></i></span><b><?= $model->eart?></b></li>
                        <li><span class = "pull-left"><i class = "glyphicon glyphicon-info-sign color-price"></i></span><b><?= $model->dop_info?></b></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-10">
            <div class="alert alert-info">Все подробности про ДОМ опубликованные на сайте</div>
        </div>
        <div class="col-sm-10 work">
            <div class="col-sm-6">
                <div class="box1">
                    <a class="view-work"
                       href="<?= Yii::getAlias('@web') .'/img/dom/' .$model->id. '/cropped/p_'.$model->avatar->path?>">
                        <img class="thumbnail image-2" alt="foto"
                             src="<?= Yii::getAlias('@web') .'/img/dom/' .$model->id. '/cropped/p_'.$model->avatar->path?>">
                    </a>
                    <h3><?= $model->price ?></h3>
                    <h4><?= $model->address ?></h4>
                </div>
            </div>

            <div class="work work-price-detail wow fadeInDown col-sm-6">
                <h3 class="text-center">Описание:</h3>
                <div class="pricing-1-box-features">
                    <ul>
                        <li>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td><span class="pull-left"><i
                                                class="fa fa-home fa-2x color-price"></i></span></td>
                                    <td colspan="2"><b><?= $model->area ?> м<sup>2</sup></b></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left"><i
                                                class="fa fa-picture-o fa-2x color-price"></i></span></td>
                                    <td><b><?= $model->eart ?></b></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left"><i class="fa fa-level-up fa-2x color-price"></i></span>
                                    </td>
                                    <td><b><?= $model->poverh ?> этажа</b></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left"><i class="fa fa-clock-o fa-2x color-price"></i></span>
                                    </td>
                                    <td><b><?= $model->readiness ?></b></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left"><i
                                                class="fa fa-map-marker fa-2x color-price"></i></span></td>
                                    <td><b><?= $model->post_adress ?></b></td>
                                </tr>
                                <tr>
                                    <td><span class="pull-left"><i
                                                class="fa fa-info-circle fa-2x color-price"></i></span></td>
                                    <td><b><?= $model->material_house ?></b></td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 contact-address">
                <div class="map">
                    <?= $this->render('moremaps', ['lat' => $model->geo_lat, 'lng' => $model->geo_lng,]) ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="page-header">Дополнительные фотографии</h3>
                </div>
                <div class="row">
                    <div class="carousel slide media-carousel" id="media">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php $i = 0;
                                    $count = count($images->models);
                                    foreach ($images->models as $image):
                                    $i++; ?>
                                    <div class="col-md-4">
                                        <a class="thumbnail view-work"
                                           href="<?= Yii::getAlias('@web') . '/img/' . $image->idDom . '/' . $image->path ?>">
                                            <img alt="" src="<?= Yii::getAlias('@web') . '/img/' . $image->idDom . '/' . $image->path ?>">
                                        </a>
                                    </div>
                                    <?php if ($i % 6 == 0) : ?>

                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if ($i % 6 == 0 && $i !== $count) : ?>
                            <div class="item">
                                <div class="row">
                                    <?php endif;
                                    ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                        <a data-slide="next" href="#media" class="right carousel-control">›</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--</div>-->


<div class="clearfix"></div>





<style>
    body {
        background: #fff;
        text-align: center;
        font-family: 'Open Sans', sans-serif;
        color: #888;
        font-size: 12px;

    }

    a {
        color: #9d426b;
        text-decoration: none;
        -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
    }
    a:hover, a:focus { color: #888; text-decoration: none; }
    strong { font-weight: bold; }
    img { max-width: 100%; }

    .work-title h2 {
        width: 220px;
        margin: 0 auto;
        background: #fff;
        font-family: 'Lobster', cursive;
        font-size: 24px;
        color: #5d5d5d;
        font-weight: bold;
    }
    .work {
        margin-top: 20px;
        padding-bottom: 20px;
        background: #f8f8f8;
        border-bottom: 2px solid #9d426b;
    }
    .work:hover img {
        opacity: 0.7;
        -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
    }
    .work:hover img.not-hover {
        opacity: 1;
        -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
    }

    .work:hover {
        box-shadow: 0 5px 15px 0 rgba(0,0,0,.05), 0 1px 25px 0 rgba(0,0,0,.05) inset, 0 -1px 25px 0 rgba(0,0,0,.05) inset;
        -o-transition: all .5s; -moz-transition: all .5s; -webkit-transition: all .5s; -ms-transition: all .5s; transition: all .5s;
    }

    .work .work-bottom {
        margin-top: 15px;
    }

    .work h3 {
        margin-top: 20px;
        padding-left: 15px;
        padding-right: 15px;
        font-family: 'Droid Sans', sans-serif;
        font-size: 14px;
        color: #5d5d5d;
        font-weight: bold;
        text-transform: uppercase;
        text-shadow: 0 1px 0 rgba(255,255,255,.7);
    }

    .work p {
        padding-left: 15px;
        padding-right: 15px;
        line-height: 24px;
        font-style: italic;
    }

    .size-style > h2{
        font-family: 'Lobster', cursive;
        font-size: 30px;
        color: #5d5d5d ;
        font-weight: bold;
    }

    .page-title-container h1 {
        display: inline;
        margin-left: 10px;
        font-family: 'Lobster', cursive;
        font-size: 24px;
        color: #5d5d5d;
        font-weight: bold;
        text-shadow: 0 1px 0 rgba(255, 255, 255, .7);
        vertical-align: middle;
    }

    .page-title-container p {
        display: inline;
        margin-left: 5px;
        font-size: 14px;
        font-style: italic;
        vertical-align: middle;
    }

    .page-title-container i {
        font-size: 46px;
        color: #ccc;
        vertical-align: middle;
    }

    .pricing-1-title h2 {
        width: 160px;
        margin: 0 auto;
        background: #fff;
        font-family: 'Lobster', cursive;
        font-size: 24px;
        color: #5d5d5d;
        font-weight: bold;
    }

    .pricing-1-box-best .pricing-1-box-price {
        background: #9d426b;
        color: #fff;
        text-shadow: none;
    }

    .pricing-1-box-price span {
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        color: #888;
        line-height: 30px;
        font-style: italic;
        text-shadow: none;
    }
    .pricing-1-box-best .pricing-1-box-price span {
        color: #fff;
    }

    .pricing-1-box h3 {
        margin-top: 0;
        margin-bottom: 0;
        padding: 10px 20px;
        background: #eee;
        font-family: 'Droid Sans', sans-serif;
        font-size: 16px;
        color: #5d5d5d;
        font-weight: bold;
        text-transform: uppercase;
        text-shadow: 0 1px 0 rgba(255,255,255,.7);
    }
    .pricing-1-box.pricing-1-box-best h3 {
        background: #9d426b;
        color: #fff;
        text-shadow: none;
    }

    .pricing-1-box h4 {
        margin-top: 0;
        margin-bottom: 2px;
        padding: 10px 20px;
        background: #e8e8e8;
        font-family: 'Droid Sans', sans-serif;
        font-size: 14px;
        color: #5d5d5d;
        line-height: 30px;
        font-weight: bold;
        text-transform: uppercase;
        text-shadow: 0 1px 0 rgba(255,255,255,.7);
    }
    .pricing-1-box.pricing-1-box-best h4 {
        background: #8d3b60;
        color: #fff;
        text-shadow: none;
    }

    .pricing-1-box-features ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .pricing-1-box-features li {
        padding: 10px 20px;
        border-bottom: 1px solid #eee;
        font-size: 14px;
        line-height: 18px;
    }

    .price-color > strong {
        font-size:25px;
    }
    .color-price, .color-them{
        color: #9d426b;
    }
    .pricing-1-box-features b{
        color: #000000;
    }
    .work.wow.fadeInDown h3{
        color: #000000;
        font-size:20px;
    }

    }
    .list.text-left > li{
        list-style-type: none;
    }
    .image-2 {
        -webkit-box-shadow:0 1px 1px rgba(0,0,0,0.2);
        -moz-box-shadow:0 1px 1px rgba(0,0,0,0.2);
        box-shadow:0 5px 5px 5px rgba(0,0,0,0.2);
    }
    .image-2:hover {
        cursor: pointer;
        opacity: 1;
        z-index: 5;
        -moz-transform: scale(1.05);
        -webkit-transform: scale(1.05);
        -o-transform: scale(1.05);
        -ms-transform: scale(1.05);
        transform: scale(1.05);
    }

    .box1 {
        position:relative;
        z-index:1;
        width:100%;
        padding:10px;
        /*  margin:20px auto;*/
        border: 1px solid rgb(200, 200, 200);
        box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
        background:  #f8f8f8;
        border-radius: 4px;
    }

    .box1 h3 {
        position:relative;
        padding:10px 30px;
        margin:0 -15px 20px;
        font-size:28px;
        line-height:32px;
        font-weight:bold;
        text-align:center;
        color:#fff !important;
        background:#9d426b;
        text-shadow:0 1px 1px rgba(0,0,0,0.2);
        -webkit-box-shadow:0 1px 1px rgba(0,0,0,0.2);
        -moz-box-shadow:0 1px 1px rgba(0,0,0,0.2);
        box-shadow:0 1px 1px rgba(0,0,0,0.2);
        zoom:1;
    }

    .box1 h3:before,.box1 h3:after {
        content:"";
        position:absolute;
        z-index:-1;
        top:100%;
        left:0;
        border-width:0 10px 10px 0;
        border-style:solid;
        border-color:transparent #913d63;
    }

    .box1 h3:after {
        left:auto;
        right:0;
        border-width:0 0 10px 10px;
    }

    .box1 h4, .black{
        color: #000000;
        font-weight:bold;
    }
    /* Previous button  */
    .media-carousel .carousel-control.left {
        left: -12px;
        background: none repeat scroll 0 0 #222222;
        border: 4px solid #FFFFFF;
        border-radius: 23px 23px 23px 23px;
        height: 40px;
        width: 40px;
        margin-top: 30px;
    }
    /* Next button  */
    .media-carousel .carousel-control.right
    {
        right: -12px !important;
        background: none repeat scroll 0 0 #222222;
        border: 4px solid #FFFFFF;
        border-radius: 23px 23px 23px 23px;
        height: 40px;
        width : 40px;
        margin-top: 30px
    }
    /* Changes the position of the indicators */
    .media-carousel .carousel-indicators
    {
        right: 50%;
        top: auto;
        bottom: 0;
        margin-right: -19px;
    }

    .contact-address {
        padding-bottom: 15px;
    }

    .contact-address .map {
        margin: 20px 0 40px 0;
        height: 300px;
        border: 5px solid #f8f8f8;
        -webkit-box-shadow:0 1px 1px rgba(0,0,0,0.2);
        -moz-box-shadow:0 1px 1px rgba(0,0,0,0.2);
        box-shadow:0 5px 5px 5px rgba(0,0,0,0.2);
    }
</style>