<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DomAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Добавить картинки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dom-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить дом', ['create'], ['class' => 'btn btn-success']) ?>
            </div>

        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'path',
                        'format' => 'html',
                        'label' => 'Фото',
                        'value' => function ($data) {
                            return '<img src=' . Yii::getAlias('@web') . '/img/dom/' . $data->idDom . '/cropped/' . $data->path . '>';
                        }
                    ],
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//                    'tech_data',
//            'text:ntext',
//                    'avatarId',
//                    'price',
                    // 'material_house',
                    // 'material_roof',
                    // 'material_windows',
                    // 'material_doors',
                    // 'material_wall',
                    // 'area',
                    // 'eart',
                    // 'address',
                    // 'dop_info',
                    // 'geo_lat',
                    // 'geo_lng',
                    // 'post_adress',
                    // 'readiness',
                    // 'poverh',
//                    'active',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view} {update}  {delete}{image}',


                        'buttons' => [
                            'image' => function ($url) {
                                return Html::a('<span class="glyphicon glyphicon-picture"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch($action){
                                case 'update':
                                    $url = \yii\helpers\Url::toRoute(['dom-image/update','id'=>$model->id]);
                                    return $url;
                                case 'view':
                                    $url = \yii\helpers\Url::toRoute(['dom-image/view','id'=>$model->id]);
                                    return $url;
                                case 'delete':
                                    $url = \yii\helpers\Url::toRoute(['dom-image/delete','id'=>$model->id]);
                                    return $url;
                                case 'image':
                                    $url = \yii\helpers\Url::toRoute(['dom-image/view','id'=>$model->id]);
                                    return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
