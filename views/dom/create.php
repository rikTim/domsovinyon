<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dom */

$this->title = 'Создать Dom';
$this->params['breadcrumbs'][] = ['label' => 'Doms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dom-create">

    <?= $this->render('_form', [
        'model' => $model,
        'upload' => $upload,
    ]) ?>

</div>
