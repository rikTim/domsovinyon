<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dom-form">

	<div class="box box-default">

		<div class="box-body">
			<div class="row">
				<div class="col-sm-12">
					<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
					<div class="col-sm-6">
						<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'eart')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>

					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'post_adress')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'poverh')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'geo_lat')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'readiness')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'material_roof')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'geo_lng')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'material_house')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'material_windows')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'material_doors')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'dop_info')->textarea(['rows' => 2]) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'material_wall')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'tech_data')->textarea(['rows' => 4]) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($model, 'active')->dropDownList(['prompt' => 'Выберите статус', '1' => 'Опубликовано', '2' => 'Убрать с сайта']) ?>
						<?= $form->field($model, 'sales')->dropDownList(['prompt' => 'Выберите статус', '1' => 'SALES', '2' => 'Продано']) ?>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<?= $form->field($upload, 'imageFile')->fileInput() ?>
							</div>
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-btn">
						<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>
				</div>

				<?php ActiveForm::end(); ?>

			</div>
		</div>

	</div>
