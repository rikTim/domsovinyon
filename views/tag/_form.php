<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tag */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container">
    <div class="row">

        <div class="tag-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'tag_news')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'category_id')->dropDownList($category) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
