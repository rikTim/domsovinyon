<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DomImageTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Dom Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dom-images-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить картинку', ['create','idDom'=>$idDom], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'dom.address',

            [
                'attribute' => 'path',
                'format' => 'html',
                'label' => 'Фото',
                'value' => function ($data) {
                    return '<img src=' . Yii::getAlias('@web') . '/img/dom/' . $data->idDom . '/cropped/' . $data->path . '>';
                }
            ],
            'number',
//            'path',
//            'active',


            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update} {delete} ',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
