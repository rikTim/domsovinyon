<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DomImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dom-images-form">

	<div class="box box-default">

		<div class="box-body">

	    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		<?= $form->field($model, 'number')->textInput() ?>

	    <?= $form->field($model, 'idDom')->hiddenInput(['value'=>$idDom])->label(false); ?>

		<?= $form->field($upload, 'imageFile')->fileInput() ?>

		</div>

	    <div class="box-footer">
			<div class="form-btn">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    	</div>
	    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
