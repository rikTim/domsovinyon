<?php

use yii\helpers\Html;
use app\models\Dom;
use app\models\DomImages;

/* @var $this yii\web\View */
/* @var $model app\models\DomImages */

$this->title = 'Обновить Доп изображение дома' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dom Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dom-images-update">

    <?= $this->render('_form', [
        'model' => $model,
        'idDom'=>$idDom,
        'upload'=>$upload,
    ]) ?>

</div>
