<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DomImages */

$this->title = 'Create Dom Images';
$this->params['breadcrumbs'][] = ['label' => 'Dom Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dom-images-create">

    <?= $this->render('_form', [
        'model' => $model,
        'idDom'=>$idDom,
        'upload'=>$upload,
    ]) ?>

</div>
