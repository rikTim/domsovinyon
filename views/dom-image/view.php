<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DomImages */

$this->title = 'View Dom Images' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dom Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dom-images-view">

    <div class="box">

        <div class="box-header with-border">
        <?= Html::a('Далее', ['index','id'=>$model->idDom], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        </div>

        <div class="box-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
            'idDom',
                [
                    'attribute'=>'path',
                    'value'=>('/img/dom/' . $model->idDom . '/cropped/' . $model->path),
                    'format' => ['image',['width'=>'180','height'=>'150']],
                ]
          //  'active',
            ],
        ]) ?>

        </div>

    </div>

</div>
