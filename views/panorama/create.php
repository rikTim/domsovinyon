<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Panorama */

$this->title = 'Создать Panorama';
$this->params['breadcrumbs'][] = ['label' => 'Panoramas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panorama-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'stutus_id'=>$status_id,
        'idDom'=>$idDom,
        'upload'=> $upload,
    ]) ?>

</div>
