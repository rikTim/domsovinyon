<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PanoramaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Panorama';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panorama-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Содать Panorama', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'dom.address',
//            'idDom'=>$idDom->adress,
            [
                'attribute'=>'name_image',
                'label'=>'Панорама',
                'format'=>'raw',
                'value'=>function($data){
                    return "<img alt=\"\" class=\"img-responsive\" src=\" ".Yii::getAlias('@web') . '/img/panorama/' . $data->avatar->idDom . '/cropped/pa_' . $data->avatar->path ."\">";
                }

            ],
//            'name_image',
            'header_image',
            'title_image',
            'alt_image',
            [
                'attribute'=>'name_image',
                'label'=>'Панарома-mini',
                'format'=>'raw',
                'value'=>function($data){
                    return "<img alt=\"\" class=\"img-responsive\" src=\" ".Yii::getAlias('@web') . '/img/panorama/' . $data->avatar->idDom . '/cropped/pm_' . $data->avatar->path ."\">";
                }

            ],
           // 'status_id',
            [
                'attribute' => 'status_id',
                'value' => function($data){
                    switch ($data->status_id){
                        case 1: return 'Черновик';
                        case 2: return 'Опубликовано';
                        default: return '';
                    }

                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'headerOptions'=> ['width' =>70],
                'template'=> '{update}&nbsp;{delete}',
            ],
        ],
    ]); ?>
</div>
