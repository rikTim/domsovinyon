<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Panorama */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panorama-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'idDom')->textInput() ?>

    <?= $form->field($model, 'idDom')->dropDownList($idDom, ['prompt' => 'Адрес дома']) ?>
<!--    --><?//= $form->field($model, 'name_image')->textInput(['maxlength' => true]) ?>
    <?= $form->field($upload, 'panorama')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'header_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alt_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_id')->dropDownList(['prompt' => 'Выберите статус', '1' => 'Черновик', '2' => 'Опубликовано']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
