<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Panorama */

$this->title = 'Обновить Panorama: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Panoramas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panorama-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--    --><?/*= $this->render('_form', [
        'model' => $model,
    ]) */?>

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <?= $form->field($upload, 'imageFile')->fileInput(['accept' => 'image/*']) ?>
    <?= $form->field($model, 'status_id')->dropDownList(['prompt' => 'Выберите статус', '1' => 'Черновик', '2' => 'Опубликовано']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php \yii\widgets\ActiveForm::end(); ?>


</div>
