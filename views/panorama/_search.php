<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PanoramaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panorama-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idDom') ?>

    <?= $form->field($model, 'name_image') ?>

    <?= $form->field($model, 'header_image') ?>

    <?= $form->field($model, 'title_image') ?>

    <?php // echo $form->field($model, 'alt_image') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
