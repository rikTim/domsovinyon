<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\DomImages;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SliderMainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главный слайдер';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 box">-->
            <div class="slider-main-index">

                <h1><?= Html::encode($this->title) ?></h1>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <p>
                    <?= Html::a('Создать слайдер', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

//                        'id',
                        [
                            'attribute'=>'images',
                            'format'=>'raw',
                            'value'=>function($data){
                                return "<img alt=\"\" class=\"img-thumbnail\" src=\" ".Yii::getAlias('@web') . '/img/slider/' . $data->avatar->idDom . 'cropped/sm_' . $data->avatar->path ."\">";
                            }
                        ],
                        'header',
//                        'stutus_id',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '70'],
                            'template' => '{view}&nbsp;{update}',
                        ],
                    ],
                ]); ?>
            </div>

        </div>

        <!--        <style>
            .view{
                background-color: #FFFFFF;
            }
        </style>

        <div class="container">
            <div class="row">
                <div class="item active">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 view">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h3>Отображаемые слайды!</h3>
                            </div>
                            <?php /* $count=1;
                            foreach ($images as $image):*/?>

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <p class="text-center thumbnail"><?php /*echo $count; $count++;  */?>
                                        <img alt="" class="img-thumbnail" src="<?/*= Yii::getAlias('@web') . '/img/slider/' . $image->avatar->idDom . 'cropped/sm_' . $image->avatar->path */?>">
                                    </p>

                                </div>
                            <?php /*endforeach; */?>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->