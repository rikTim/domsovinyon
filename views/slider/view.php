<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SliderMain */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Slider Mains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-main-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Далее', ['slider/index'], ['class' => 'btn btn-primary']) ?>

    </p>


</div>


<div class="container">
    <div class="row">
        <div class="item active">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Отобразиться слайд!</h3>
                    </div>
                    <div class="col-sm-5">
                        <img alt="" class="thumbnail view-work text-center" src="<?= Yii::getAlias('@web') . '/img/slider/cropped/sb_' . $model->avatar->path ?>">
                        <h4 class="text-left opened"><?=$model->header ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


