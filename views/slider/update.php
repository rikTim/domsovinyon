<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SliderMain */

$this->title = 'Обновить слайдер: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Главный слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="slider-main-update">

    <h1><?= Html::encode($this->title) ?></h1>



    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 box">

                <div class="item active">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3>Размер изображения 1200X550 обрезать можете <a href="http://www.fotor.com/ru/app.html#!module/basic/tool/BasicEdits" target="_blank">тут</a> </h3>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <!--                        <div class="col-sm-6">-->
                            <!--                            --><?php
                            //                            foreach ($images as $image):?>
                            <!---->
                            <!--                                <div class="col-sm-3 box1">-->
                            <!--                                    <a class="thumbnail" href="">-->
                            <!--                                        <img alt="" src="--><?//= Yii::getAlias('@web') . '/img/dom/' . $image->id . '/cropped/p_' . $image->avatar->path ?><!--">-->
                            <!--                                    </a>-->
                            <!--                                    <p class="text-left">-->
                            <!--                                        <i class="fa fa-plus-square-o fa-2x avatar-success" data-avatar="--><?//= $image->avatarId ?><!--" data-path="--><?//= $image->avatar->path ?><!--"></i>-->
                            <!--                                    </p>-->
                            <!--                                </div>-->
                            <!---->
                            <!--                            --><?php //endforeach; ?>
                            <!--                        </div>-->

                            <div class="col-sm-6">
                                <div class="slider-main-form">
                                    <!--                                <div class="row">-->
                                    <!--                                    <div class="col-sm-3">-->
                                    <!--                                        <div id="success">-->
                                    <!--                                            <div id="my-photo">-->
                                    <!--                                                <img  class="thumbnail" alt="" src="--><?//= Yii::getAlias('@web') . '/img/dom/' . $model->avatar->idDom . '/cropped/p_' . $model->avatar->path ?><!--">-->
                                    <!--                                            </div>-->
                                    <!--                                        </div>-->
                                    <!--                                    </div>-->
                                    <!--                                </div>-->
                                    <?php $form = \yii\widgets\ActiveForm::begin(); ?>
                                    <!--                    --><? //= $form->field($model, 'id')->textInput() ?>
                                    <?= $form->field($upload, 'imageFile')->fileInput(['accept' => 'image/*']) ?>

                                    <div class="form-group">
                                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                    </div>
                                    <?php \yii\widgets\ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .box1 {
            border: 1px solid rgba(0, 0, 0, 0.15);
            margin:10px;
        }
    </style>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script>
        (function ($) {
            var o = $('.avatar-success');
            if (o) {
                $(function () {

                    o.on('click', function () {
                        var avatar = $(this).data('avatar');
                        var url = 'index.php?r=slider%2Fphoto&id=' + avatar;
                        $('#avatar-finish').val(avatar);
                        $("#my-photo").remove();
                        $.ajax({
                            url: url,
                            type: "GET",
                            success: function (data) {
                                $('#success').append(data);
                            }
                        });

                    });
                })
            }
        })(jQuery);

    </script>-->


</div>
