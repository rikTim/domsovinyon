<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SliderMain */

$this->title = 'Создать главный слайдер';
$this->params['breadcrumbs'][] = ['label' => 'Главный слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-main-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'upload' => $upload,
        'model' =>$model,
    ]) ?>

</div>
