<?php
namespace app\components;

use yii\base\Component;

class DLL extends Component
{
    public $content;

    public function init()
    {
        parent::init();
    }

    // Yii::$app->DLL->methodName();

    public static function subStr($str, $len = 400)
    {
        if (strlen($str) < $len) {
            return $str;
        }
        $subStr = substr($str, 0, $len);
        return substr($subStr, 0, strrpos($subStr, ' ')) . ' ...';
    }

}