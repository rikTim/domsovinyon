
jQuery(document).ready(function() {
	
	/*
	    Wow
	*/
	new WOW().init();
	
	/*
	    Slider
	*/
/*	$('.flexslider').flexslider({
        animation: "fade",
        controlNav: "thumbnails",
        prevText: "",
        nextText: ""
    });*/

	/*
	    Image popup (home latest work)
	*/
	$('.view-work').magnificPopup({
		type: 'image',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: 'The image could not be loaded.',
			titleSrc: function(item) {
				return item.el.parent('.work-bottom').siblings('img').attr('alt');
			}
		},
		callbacks: {
			elementParse: function(item) {
				item.src = item.el.attr('href');
			}
		}
	});
	

	/*
	    Google maps
	*/
/*	var position = new google.maps.LatLng(45.067883, 7.687231);
    $('.map').gmap({'center': position,'zoom': 15, 'disableDefaultUI':true, 'callback': function() {
            var self = this;
            self.addMarker({'position': this.get('map').getCenter() });	
        }
    });*/

	
});
