function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}

var path = location.pathname.split('/');
//window.baseUrl = location.protocol + '//' + location.hostname + ':' + location.port + '/' + path[1] ;
window.baseUrl = location.protocol + '//' + location.hostname + '/';
(function ($) {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });
    var o = $('#callme-form');

    if (o) {
        var url = window.baseUrl +'site/callme?type=1';
        $.ajax({
            url: url,
            type: "GET",
            success: function (data) {
                $('#callme-form').append(data);
            }
        });
    }

    $(document).on("submit", '.call-me-form', function (e) {
        e.preventDefault();
        var form = $(this);
// console.log(1);
        var url = window.baseUrl +'site/callme?type=1';

        $.ajax({
            url: url,
            type: "POST",
            data: form.serialize(),
            success: function (result) {
                if (result == 'true') {
                    // console.log(1);
                    var modalContainer = $('#call-me-success');
                    modalContainer.modal({show: true});
                }
            }
        });
    });

    $(document).ready(function () {

        $('.signup').click(function (event) { //  модальное окно
            event.preventDefault();
            var url = window.baseUrl +'site/callme?type=2';
            var modalContainer = $('#my-modal');
            var modalBody = $('#call-me-body');
            modalContainer.modal({show: true});
            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    modalBody.append(data);
                    modalContainer.modal({show: true});
                }
            });
        });


    });

    $(document).on("submit", '.call-me-big', function (e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: window.baseUrl +"site/callme?type=2",
            type: "POST",
            data: form.serialize(),
            success: function (result) {
                // console.log(result);
                var modalContainer = $('#my-modal');
                var modalBody = modalContainer.find('.modal-body');
                var insidemodalBody = modalContainer.find('.gb-user-form');

                if (result == 'true') {
                    insidemodalBody.html(result).hide(); //
                    //$('#my-modal').modal('hide');
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').append("<strong>Спасибо! Ваше сообщение отправлено.</strong>");
                    $('#success > .alert-success').append('</div>');

                    setTimeout(function () { // скрываем modal через 4 секунды
                        $("#my-modal").modal('hide');
                    }, 4000);
                }
                else {
                    modalBody.html(result).hide().fadeIn();
                }
            }
        });
    });
    /*
     Contact form

     $('.contact-form form').submit(function(e) {
     e.preventDefault();

     var form = $(this);
     var nameLabel = form.find('label[for="contact-name"]');
     var emailLabel = form.find('label[for="contact-email"]');
     var messageLabel = form.find('label[for="contact-message"]');

     nameLabel.html('Name');
     emailLabel.html('Email');
     messageLabel.html('Message');

     var postdata = form.serialize();

     $.ajax({
     type: 'POST',
     url: 'index.php?r=site%2Fsendmail.php',
     data: postdata,
     dataType: 'json',
     success: function(json) {
     if(json.nameMessage != '') {
     nameLabel.append(' - <span class="violet error-label"> ' + json.nameMessage + '</span>');
     }
     if(json.emailMessage != '') {
     emailLabel.append(' - <span class="violet error-label"> ' + json.emailMessage + '</span>');
     }
     if(json.messageMessage != '') {
     messageLabel.append(' - <span class="violet error-label"> ' + json.messageMessage + '</span>');
     }
     if(json.nameMessage == '' && json.emailMessage == '' && json.messageMessage == '') {
     form.fadeOut('fast', function() {
     form.parent('.contact-form').append('<p><span class="violet">Thanks for contacting us!</span> We will get back to you very soon.</p>');
     });
     }
     }
     });
     });
     */
    // $(document).ready(function ($) {
    //     $('.cycle').cyclotron();
    // });
    $(document).ready(function(){
        $(".panorama").panorama_viewer({
            repeat: true
        });
    });

    $(document).ready(function() {
        $('#media').carousel({
            pause: true,
            interval: false,
        });
    });

    /*  $(document).ready(function(){
     $('.thickbox').fancybox();
     });
     $(document).ready(function(){
     // $("img.advancedpanorama").panorama({
     auto_start: 0,
     start_position: 1527
     });
     });*/
    // var clock;
    // var today =new Date();
    // var data_stop= new Date("2017,02,14");
    // var data_start=today.getTime();
    // var data_end=data_stop.getTime();
    //
    // var result=(new Date(data_end).getTime() - new Date(data_start).getTime())/1000;
    // $(document).ready(function() {
    //     var clock = $('.clock').FlipClock(3600 * 24 * 3, {
    //         clockFace: 'DailyCounter',
    //         countdown: true,
    //         // showSeconds: false,
    //     });
    //     clock.setTime(result);
    //     clock.setCountdown(true);
    //     clock.start();
    //
    // });
})(jQuery);
