<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
/*class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}*/
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://fonts.googleapis.com/css?family=Open+Sans:400italic,400',
        'http://fonts.googleapis.com/css?family=Droid+Sans',
        'http://fonts.googleapis.com/css?family=Lobster',
        'assets/font-awesome/css/font-awesome.min.css',
        'assets/flexslider/flexslider.css',
        'css/animate.css',
        'css/magnific-popup.css',
        'css/form-elements.css',
        'css/style.css',
        'css/media-queries.css',
        'css/panorama_viewer.css',
        'css/timeTo.css',
//        'css/flipclock.css',
/*        'panorama2/demos/css/jquery.fancybox-1.3.1.css',
        'panorama2/demos/css/jquery.panorama.css',*/
    ];
    public $js = [
        'assets/js/wow.min.js',
        'assets/js/jquery.magnific-popup.min.js',
        'assets/flexslider/jquery.flexslider-min.js',
        'assets/js/scripts.js',
        'js/app.js',
//        'panorama/js/jquery.cyclotron.js',
        'js/jquery.panorama_viewer.js',
        'js/jquery.time-to.js',
//        'js/flipclock.js',
        /*        'panorama2/demos/js/cvi_text_lib.js',
                'panorama2/jquery.advanced-panorama.js',
                'panorama2/demos/js/jquery.flipv.js',
                'panorama2/demos/js/jquery.fancybox-1.3.1.pack.js',
                'panorama2/jquery.panorama.js',*/
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
//        '\rmrevin\yii\fontawesome\AssetBundle',
//        'iutbay\yii2fontawesome\FontAwesomeAsset',
    ];
}
