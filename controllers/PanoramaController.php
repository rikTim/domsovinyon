<?php

namespace app\controllers;

use app\models\Dom;
use Yii;
use app\models\Panorama;
use app\models\PanoramaSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DomImages;
use app\models\UploadForm;
use yii\web\UploadedFile;


/**
 * PanoramaController implements the CRUD actions for Panorama model.
 */
class PanoramaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','login','logout','photo'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','photo'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['*'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin.php';
            return true;
        }
        return false;
    }
    /**
     * Lists all Panorama models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PanoramaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $images=Panorama::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'images' =>$images,
        ]);
    }

    /**
     * Displays a single Panorama model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $images=Panorama::find()->all;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'images'=>$images,
        ]);
    }

    /**
     * Creates a new Panorama model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Panorama();
        $upload=new UploadForm();
        $upload->scenario='panorama';

        if ($model->load(Yii::$app->request->post())) {
             $model->save();
//            print_r($model->getErrors());
            $upload->panorama = UploadedFile::getInstance($upload, 'panorama');
            if(!empty($upload->panorama)){
                $upload->panorama($model->id,$model->idDom);
            }
            return $this->redirect(['index']);
//            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'idDom' =>  ArrayHelper::map(Dom::find()->all(), 'id', 'address'),
                'upload'=>$upload,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Panorama model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload=new UploadForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->imageFile=UploadedFile::getInstance($upload, 'imageFile');
            if(!empty($upload->imageFile)){
               $upload->panorama($model->id, $model->idDom);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'idDom' =>  ArrayHelper::map(Dom::find()->all(), 'id', 'address'),
                'model' => $model,
                'upload'=>$upload,
            ]);
        }
    }

    /**
     * Deletes an existing Panorama model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Panorama model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Panorama the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Panorama::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionPhoto($id)
    {
        $image = DomImages::find()->where(['id'=> $id])->one();
        return $this->renderPartial('photo',[
            'image'=>$image
        ]);
    }
}
