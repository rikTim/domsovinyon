<?php

namespace app\controllers;

use app\models\DomImages;
use app\models\DomImagesSearch;
use app\models\UploadForm;
use Yii;
use app\models\Dom;
use app\models\DomAdminSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DomController implements the CRUD actions for Dom model.
 */
class DomController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','image','login','logout'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','image','imagemore'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin.php';


            return true;
        }
        return false;
    }

    /**
     * Lists all Dom models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new DomAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dom model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $files = DomImages::find()->where(['idDom' => $model->id])->all();
        $searchModel = new DomImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id,$model->avatarId);
        return $this->render('view', [
            'model' =>   $model,
            'images' => $dataProvider,
            'files' =>$files,
        ]);
    }

    /**
     * Creates a new Dom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dom();
        $upload = new UploadForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $upload->avatar($model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Updates an existing Dom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->save();
//            print_r($model->getErrors());die();
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $upload->avatar($model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Deletes an existing Dom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionImage($id)
    {
        $model = $this->findModel($id);

        $upload = new UploadForm();

        $photos = DomImages::find()->where(['idDom'=> $id])->andWhere('id' != $model->avatarId)->all();

        if ( $model->load(Yii::$app->request->post()) ) {
//            print_r(1);die();
            $upload->files = UploadedFile::getInstances($upload, 'files');
            if (!empty($upload->files)) {
                $upload->photos($model->id);
            }
            $this->redirect(['image','id'=>$id]);
        }

        return $this->render('image', [
            'model' => $model,
            'upload' => $upload,
            'photos' =>$photos,
        ]);
    }


    public function actionImagemore($id)
    {

        $model = $this->findModel($id);
        $searchModel = new DomImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id,$model->avatarId);

        return $this->render('imagemore', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
