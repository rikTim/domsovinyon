<?php

namespace app\controllers;

use app\models\Category;
use app\models\Dom;
use app\models\DomImagesSearch;
use app\models\SignupForm;
use app\models\DomSearch;
use app\models\News;
use app\models\NewsSearch;
use app\models\Panorama;
use app\models\Promo;
use app\models\UserInfo;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\TagSearch;




class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['login', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($role == 'admin') {
                return $this->redirect(['admin/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],

        ];

    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $promoSearch = new Promo();
        $promo = $promoSearch->getPromo();
        $searchModel = new DomSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'promo' => $promo,
            'model' => new UserInfo(),
        ]);

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->redirect(['admin/login']);
    }

    public function actionMore($id)
    {
        $promoSearch = new Promo();
        $promo = $promoSearch->getPromo();
        $dom = Dom::findOne($id);
        $searchModel = new DomImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id,$dom->avatarId);
//        printr_r($promo);die();
        return $this->render('more', [
            'dom' => $dom,
            'promo' => $promo,
            'images' => $dataProvider,
            'model' => new UserInfo(),
        ]);
    }

    /*3d tour*/
    public function actionPanorama($id)
    {
        $dom = Dom::findOne($id);
        $panorama = Panorama::find()->where(['idDom'=> $id,'status_id'=>2])->all();
        return $this->render('panorama', [
            'dom' => $dom,
            'images' => $panorama,
            'model' => new UserInfo(),
        ]);
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new UserInfo();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $message = $model->name.' оставил заявку на тему '.$model->subject.'. Текст сообщения: '.$model->text.'. Мои контакты телефон: '.$model->telefone.' , email: '.$model->email.'.';
            Yii::$app->session->setFlash('success', 'Сообщение отправлено. В ближайшее время мы ответим Вам.');
            Yii::$app->mailer->compose()
                ->setTo('synytsina@mail.ru')
                ->setFrom('to@domsovinyon.com')
                ->setSubject('Тема сообщения')
                ->setTextBody('Текст сообщения')
                ->setHtmlBody($message)
                ->send();
            return $this->refresh();
        }else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about',[
            'model' => new UserInfo(),
        ]);

    }

    public function actionCallme($type)
    {
        $model = new UserInfo();
        switch($type){
            case 1: $render = '_callme';break;
            case 2: $render = 'callme';break;
        }

        if($model->load(Yii::$app->request->post()) && $model->save() ) {
            $message = $model->name.' оставил заявку.Пожалуйста свяжитесь со мной по телефону'.$model->telefone;
            $success = true;
            Yii::$app->mailer->compose()
                ->setTo('konrun@mail.ru')
                ->setFrom('to@domsovinyon.com')
                ->setSubject('Тема сообщения')
                ->setTextBody('Текст сообщения')
                ->setHtmlBody($message)
                ->send();
            return json_encode($success);
        }

        return $this->renderPartial($render, [
            'model' => $model,
        ]);


    }
    public function actionProject()
    {
        return $this->render('project',[
            'model' => new UserInfo(),
        ]);
    }
    public function actionMaterial()
    {
        return $this->render('material',[
            'model' => new UserInfo(),
        ]);

    }

    public function actionTehnology()
    {
        return $this->render('tehnology',[
            'model' => new UserInfo(),
        ]);

    }

    public function actionDocumenty()
    {
        return $this->render('documenty',[
            'model' => new UserInfo(),
        ]);
    }
    public function actionNews()
    {
        $category = Category::find()->all() ;
        $searchModelTag = new TagSearch();
        $dataProviderTag = $searchModelTag->search(Yii::$app->request->queryParams);
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderLast = $searchModel->search(Yii::$app->request->queryParams,1);
        return $this->render('news',[
            'model' => new UserInfo(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category,
            'dataProviderLast' => $dataProviderLast,
            'dataProviderTag' => $dataProviderTag,
        ]);
    }

    public function actionCallmePopup()
    {
        $model = new SignupForm();
        return $this->renderPartial('signup', [
            'model' => $model,
        ]);
    }

    public function actionNewsMore ($id) {
        $searchModelTag = new TagSearch();
        $dataProviderTag = $searchModelTag->search(Yii::$app->request->queryParams);
        $category = Category::find()->all() ;
        $searchModel = new NewsSearch();
        $dataProviderLast = $searchModel->search(Yii::$app->request->queryParams,1);
        $model = new News();
        $news = $model->findOne($id) ;
        return $this->render('news-more', [
            'news' => $news,
            'model' => new UserInfo(),
            'category' => $category,
            'dataProviderLast' => $dataProviderLast,
            'dataProviderTag' => $dataProviderTag,
        ]);

    }

// Всплывшее модальное окно заполняем представлением signup.php формы с полями
    public function actionSignup()
    {
        $model = new SignupForm();
        //$model->id =$userid;
        return $this->renderPartial('callme', [
            'model' => $model,
        ]);
    }

// По нажатию в модальном окне на Отправить - форма отправляется администратору на почту
    public function actionSubmitsignup()
    {
        $model = new SignupForm();
        $model->load(Yii::$app->request->post());

        if($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {

            //save the password
            $success=true;
            return json_encode($success);
        }
        else
        {
            return $this->renderPartial('signup', [
                'model' => $model,
            ]);
        }
    }

}
