<?php

namespace app\controllers;

use app\models\Dom;
use app\models\DomImages;
use app\models\UploadForm;
use Yii;
use app\models\SliderMain;
use app\models\SliderMainSearch;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for SliderMain model.
 */
class SliderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','image','login','logout','photo'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','photo'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['*'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin.php';
            return true;
        }
        return false;
    }
    /**
     * Lists all SliderMain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderMainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $images = SliderMain::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'images' =>$images,
        ]);
    }

    /**
     * Displays a single SliderMain model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $images = SliderMain::find()->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'images' =>$images,
        ]);
    }

    /**
     * Creates a new SliderMain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SliderMain();
        $upload = new UploadForm();
        $upload -> scenario = 'slider';
        if ($model->load(Yii::$app->request->post())) {
                $upload->slider = UploadedFile::getInstances($upload, 'slider');
                if (!empty($upload->slider)) {
//                    echo 1;die();
                    $upload->slider();
                }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'upload' => $upload,
                'model'=> $model,
            ]);
        }
    }

    /**
     * Updates an existing SliderMain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if (!empty($upload->imageFile)) {
//                    echo 1;die();
                $upload->oneSlide($model->id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload'=>$upload,
            ]);
        }
    }

    /**
     * Deletes an existing SliderMain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SliderMain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SliderMain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SliderMain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPhoto($id)
    {
        $image = DomImages::find()->where(['id'=> $id])->one();
        return $this->renderPartial('photo',[
            'image'=>$image
        ]);
    }
}
