<?php

namespace app\controllers;

use app\models\Dom;
use app\models\DomImagesSearch;
use app\models\UploadForm;
use Yii;
use app\models\DomImages;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * DomImageController implements the CRUD actions for DomImages model.
 */
class DomImageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'.'view','create','update','delete','image','login','logout'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','image','imagemore'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['*'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin.php';


            return true;
        }
        return false;
    }


    /**
     * Lists all DomImages models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $model = Dom::findOne($id);
        $searchModel = new DomImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id,$model->avatarId);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idDom'=>$id
        ]);
    }

    /**
     * Displays a single DomImages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DomImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idDom)
    {
        $model = new DomImages();
        $upload = new UploadForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $upload->photo($idDom);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload'=>$upload,
                'idDom'=>$idDom,
            ]);
        }
    }

    /**
     * Updates an existing DomImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        $idDom = DomImages::findAll($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $upload->photo($model->idDom);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload'=>$upload,
                'idDom'=> $idDom,
            ]);
        }
    }

    /**
     * Deletes an existing DomImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $idDom = $model->idDom;
        $model->delete();

        return $this->redirect(['index','id'=>$idDom]);
    }

    /**
     * Finds the DomImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DomImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DomImages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
